# Utah Healthcare Facility Database (HFD) Data User Manual
February 2022

## Table of Contents

- [Revision History](#revision-history)
- [Introduction](#introduction)
- [Limited Use Data Sets](#limited-use-data-sets)
- [Data Processing and Quality](#data-processing-and-quality)
    - [Data Submission](#data-submission)
    - [System Edits](#system-edits)
    - [Hospital Review](#hospital-review)
    - [Missing Values](#missing-values)
    - [Patient Confidentiality](#patient-confidentiality)
    - [DRG, MS-DRG, APR-DRG, and EAPG Classification](#drg-ms-drg-apr-drg-and-eapg-classification)
    - [Citation](#citation)
- [Available Data and Fill Rates](#available-data-and-fill-rates)
    - [Ambulatory Surgery](#ambulatory-surgery)
    - [Emergency Department](#emergency-department)
    - [Inpatient](#inpatient)
- [Variable Descriptions](#variable-descriptions)
    - [Record_ID](#record_id)
    - [Encounter_Type](#encounter_type)
    - [Facility_Number](#facility_number)
    - [Facility_Name](#facility_name)
    - [Age_Group_Num](#age_group_num)
    - [Age_Group](#age_group)
    - [Bill_Type](#bill_type)
    - [ER_Flag](#er_flag)
    - [Observation_Stay_Flag](#observation_stay_flag)
    - [Year](#year)
    - [Quarter](#quarter)
    - [Patient_Medical_Record_Number](#patient_medical_record_number)
    - [Patient_Social_Security_Number](#patient_social_security_number)
    - [Patient_Social_Security_Flag](#patient_social_security_flag)
    - [Patient_Unique_Number](#patient_unique_number)
    - [Patient_Control_Number](#patient_control_number)
    - [Patient_Name_Last](#patient_name_last)
    - [Patient_Name_First](#patient_name_first)
    - [Patient_Name_Middle](#patient_name_middle)
    - [Patient_Name_Suffix](#patient_name_suffix)
    - [Patient_Street_Address](#patient_street_address)
    - [Patient_City](#patient_city)
    - [Patient_State](#patient_state)
    - [Patient_County_FIPS](#patient_county_fips)
    - [Patient_Zip_Code](#patient_zip_code)
    - [Patient_County](#patient_county)
    - [Patient_Country](#patient_country)
    - [Patient_Date_of_Birth](#patient_date_of_birth)
    - [Patient_Gender](#patient_gender)
    - [Patient_Marital_Status](#patient_marital_status)
    - [Patient_Race](#patient_race)
    - [Patient_Ethnicity](#patient_ethnicity)
    - [Patient_Race_Ethnicity](#patient_race_ethnicity)
    - [Admission_Date](#admission_date)
    - [LOS_Days](#los_days)
    - [LOS_Hours](#los_hours)
    - [Admission_Hour](#admission_hour)
    - [Admission_Minute](#admission_minute)
    - [Admission_Type](#admission_type)
    - [Admission_Source](#admission_source)
    - [Discharge_Hour](#discharge_hour)
    - [Discharge_Minute](#discharge_minute)
    - [Patient_Discharge_Status](#patient_discharge_status)
    - [Patient_Discharge_Status_Uniform](#patient_discharge_status_uniform)
    - [Statement_Beginning_Date](#statement_beginning_date)
    - [Statement_Through_Date](#statement_through_date)
    - [Condition_Code](#condition_code)
    - [Total_Charge_Header](#total_charge_header)
    - [Payer_Name](#payer_name)
    - [Payer_ID](#payer_id)
    - [Payer_Typology](#payer_typology)
    - [Payer_Typology_Imputed](#payer_typology_imputed)
    - [Estimated_Amount_Due](#estimated_amount_due)
    - [Prior_Payment](#prior_payment)
    - [Insured_Unique_ID](#insured_unique_id)
    - [Insured_Name_Last](#insured_name_last)
    - [Insured_Name_First](#insured_name_first)
    - [Insured_Name_Middle](#insured_name_middle)
    - [Insured_Name_Suffix](#insured_name_suffix)
    - [Insured_Group_Name](#insured_group_name)
    - [Patient_Relationship](#patient_relationship)
    - [Diagnosis_Version_Flag](#diagnosis_version_flag)
    - [Diagnosis_Code](#diagnosis_code)
    - [Diagnosis_Code_POA](#diagnosis_code_poa)
    - [Admitting_Diagnosis_Code](#diagnosis_code)
    - [Patient_Reason_For_Visit](#patient_reason_for_visit)
    - [E_Code](#e_code)
    - [E_Code_POA](#e_code_poa)
    - [ICD_Procedure](#icd_procedure)
    - [Procedure_Date](#procedure_date)
    - [Diagnosis_Related_Group](#diagnosis_related_group)
    - [Provider_ID](#provider_id)
    - [Provider_NPI](#provider_npi)
    - [Provider_ID_Qualifier](#provider_id_qualifier)
    - [Provider_Taxonomy_Code](#provider_taxonomy_code)
    - [Provider_AMA_Specialty](#provider_ama_specialty)
    - [Provider_Other_Type_Qualifier](#provider_other_type_qualifier)
    - [Data_Source](#data_source)
    - [Service_Line](#service_line)
    - [Revenue_Code](#revenue_code)
    - [HCPCS_Code](#hcpcs_code)
    - [HCPCS_Code_Mod](#hcpcs_code_mod)
    - [National_Drug_Code](#national_drug_code)
    - [Service_Line_Date](#service_line_date)
    - [Service_Units](#service_units)
    - [Unit_Of_Measurement](#unit_of_measurement)
    - [Total_Charge_By_Revenue_Code](#total_charge_by_revenue_code)
    - [ER_Charges](#er_charges)
    - [Facility_Charges](#facility_charges)
    - [Professional_Charges](#professional_charges)
    - [MSGMCE_Version_Used](#msgmce_version_used)
    - [Initial_DRG](#initial_drg)
    - [Initial_MS_Indicator](#initial_ms_indicator)
    - [Final_MDC](#final_mdc)
    - [Final_DRG](#final_drg)
    - [Final_MS_Indicator](#final_ms_indicator)
    - [DRG_Return_Code](#drg_return_code)
    - [MSGMCE_Edit_Return_Code](#msgmce_edit_return_code)
    - [Initial_4digit_DRG](#initial_4digit_drg)
    - [Final_4digit_DRG](#final_4digit_drg)
    - [Final_DRG_CC_MCC_Usage](#final_drg_cc_mcc_usage)
    - [Initial_DRG_CC_MCC_Usage](#initial_drg_cc_mcc_usage)
    - [Hospital_Acquired_Conditions](#hospital_acquired_conditions)
    - [Hospital_Acquired_Condition_Status](#hospital_acquired_condition_status)
    - [Cost_Weight](#cost_weight)

## Revision History

|     Date      |  Description  | Author(s) |
| ------------- | ------------- | --------- |
| November 2019 | Initial Draft | Petersen  |
| February 2020 | Minor edits   | Petersen  |
| March 2020    | Minor edits   | Petersen  |
| February 2022 | Fill rate table changes & minor edits to better reflect VRW | Scott | 

## Introduction

The Utah Health Data Committee is composed of fifteen governor-appointed members and was
created by the Utah Health Data Authority Act of 1991. The Committee is staffed by the
Office of Health Care Statistics which manages the Utah Healthcare Facility Database.

Utah Administrative Rule requires all Utah licensed hospitals, both general acute care and
specialty, and free standing ambulatory surgical centers to provide data on inpatient,
emergency department, and ambulatory surgery encounters. The Healthcare Facility Database
contains information on patient demographics, admission and discharge, diagnoses, services
received, and charges billed for each encounter.

Data submissions by the FASCs may be incomplete and caution should be used when trying to
perform market level comparisons with these data. Continual efforts are being made to
further data completeness.

Starting in 2016, Huntsman Cancer Institute, Madsen Surgery Center, Moran Eye Center, and
University Orthopedic Center are the only University of Utah Health sites reported
individually; all other University Hospital & Clinics are reported as one facility (#125).

## Limited Use Data Sets

Limited use data sets are available for inpatient, emergency department, and
ambulatory surgery encounters. The limited use data sets are designed to provide general
health care information to a wide spectrum of users with minimal controls.

The limited use data sets include data on procedures, charges, and length of stay. Several factors,
such as case-mix, severity complexity, payer-mix, market areas, hospital ownership,
hospital affiliation, or hospital teaching status, affect the comparability of charge and
length of stay across hospitals. Any analysis of charge or length of stay at the hospital
level should consider the above factors. More information about hospitals can be found in
the [Health Facility Licensing website](http://health.utah.gov/hflcra/facinfo.php).

## Data Processing and Quality

### Data Submission

The Office of Health Care Statistics maintains and publishes the Utah Healthcare Facility
Data Submission Guide on its website. Data suppliers submit all files using specifications
in the data submission guide.

### System Edits

The data are validated through a process of automated editing and report verification.
Each record is subjected to a series of edits that check for validity, consistency,
completeness, and conformity with the definitions specified in the Utah Healthcare
Facility Data Submission Guide. Files that fail edit checks are returned to the data
supplier for correction.

### Hospital Review

Each hospital is given the opportunity to review and validate findings of the edit checks
and any public report prior to the release of data or information. Inconsistencies
discovered by the facilities are reevaluated or corrected.

### Missing Values

When dealing with unknown values, it is important to distinguish between systematic
omission by the facility (e.g., for facilities that were granted reporting exemption for
particular data elements or which had coding problems that deemed the entire data from the
facility unusable) and non-systematic omission (e.g., coding problems, invalid codes,
etc.). While systematic omission creates potential bias, non-systematic omission is
assumed to occur randomly. The user is advised to examine missing values by facility for
each data element to be used. The user is likewise advised to examine the number of
observations by facility by quarter to judge if a facility under-reported for a given
quarter, which occasionally happens due to data processing problems experienced by a
facility.

### Patient Confidentiality

The Committee has taken steps to ensure that no individual patient will be identified from
the limited use data sets. Patient’s age, physician specialty, and payers are grouped.
Several data elements are suppressed under specific conditions: 1) ZIP codes with less
than 30 visits in a calendar year are suppressed; 2) physician taxonomy is suppressed for
hospitals with less than 30 beds; 3) Payer names with less than 30 visits in a calendar 
year are suppressed; and 4) age, sex, and ZIP code are suppressed if the discharge involves
substance abuse or HIV infection, as defined by the following Medicare Severity Grouper
Diagnosis Related Groups (MS-DRGs):
 - 894—ALCOHOL, DRUG ABUSE OR DEPENDENCE, LEFT AMA
 - 895—ALCOHOL, DRUG ABUSE OR DEPENDENCE WITH REHABILITATION THERAPY
 - 896—ALCOHOL, DRUG ABUSE OR DEPENDENCE WITHOUT REHABILITATION THERAPY WITH MCC
 - 897—ALCOHOL, DRUG ABUSE OR DEPENDENCE WITHOUT REHABILITATION THERAPY WITHOUT MCC
 - 969—HIV WITH EXTENSIVE O.R. PROCEDURE WITH MCC
 - 970—HIV WITH EXTENSIVE O.R. PROCEDURE WITHOUT MCC
 - 974—HIV WITH MAJOR RELATED CONDITION WITH MCC
 - 975—HIV WITH MAJOR RELATED CONDITION WITH CC
 - 976—HIV WITH MAJOR RELATED CONDITION WITHOUT CC/MCC
 - 977—HIV WITH OR WITHOUT OTHER RELATED CONDITION

### DRG, MS-DRG, APR-DRG, and EAPG Classification

Variables produced by OHCS using 3M grouper software are no longer standard inclusions in
the limited use data sets. Maintenance of the DRG grouper was discontinued in 2007.
Previous versions of limited use data sets may have included variables resulting from the
DRG grouper to aid comparisons to historical data. However, this grouper cannot be applied
to data beginning in 2015 due to the change from ICD-9 to ICD-10 and thus is no is no
longer included in the limited use data sets.

The MS-DRG grouper results are the only grouper results available.

### Citation

Any statistical reporting or analysis based on the data shall cite the source as the
following:

_Utah Healthcare Facility Limited Use Data Sets (2022)_. Utah Health Data Committee/Office
of Health Care Statistics. Utah Department of Health. Salt Lake City, Utah. 2022.


## Available Data and Fill Rates

Fill rates are based on source variables contained in the database. Empty rows are generally
for calculated fields. For a fill rate, see the variable(s) used to calculated the field.

### Ambulatory Surgery

|                               Variable Name                               | Standard Limited-Use Variable | 1996 | 1997 | 1998 | 1999 | 2000 | 2001 | 2002 | 2003 | 2004 | 2005 | 2006 | 2007 | 2008 | 2009 | 2010 | 2011 | 2012 | 2013 | 2014 | 2015 | 2016 | 2017 | 2018 |
| ------------------------------------------------------------------------- | ----------------------------- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| [Record_ID](#record_id)                                                   | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Encounter_Type](#encounter_type)                                         | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Facility_Number](#facility_number)                                       | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Facility_Name](#facility_name)                                           | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Age_Group_Num](#age_group_num)                                           | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Age_Group](#age_group)                                                   | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Bill_Type](#bill_type)                                                   |                               | 100% | 100% | 98%  | 98%  | 99%  | 99%  | 99%  | 99%  | 99%  | 99%  | 99%  | 99%  | 99%  | 99%  | 98%  | 98%  | 97%  | 97%  | 97%  | 99%  | 100% | 100% | 100% |
| [ER_Flag](#er_flag)                                                       | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Observation_Stay_Flag](#observation_stay_flag)                           | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Year](#year)                                                             | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Quarter](#quarter)                                                       | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Patient_Medical_Record_Number](#patient_medical_record_number)           |                               | 0%   | 0%   | 94%  | 34%  | 29%  | 32%  | 38%  | 37%  | 38%  | 40%  | 40%  | 41%  | 43%  | 43%  | 42%  | 43%  | 42%  | 40%  | 99%  | 99%  | 99%  | 100% | 100% |
| [Patient_Social_Security_Number](#patient_social_security_number)         |                               | 81%  | 79%  | 83%  | 88%  | 90%  | 91%  | 92%  | 92%  | 92%  | 92%  | 92%  | 91%  | 90%  | 89%  | 86%  | 89%  | 89%  | 88%  | 84%  | 86%  | 82%  | 82%  | 85%  |
| [Patient_Social_Security_Flag](#patient_social_security_flag)             |                               | 8%   | 10%  | 10%  | 9%   | 8%   | 8%   | 7%   | 7%   | 7%   | 6%   | 7%   | 8%   | 9%   | 10%  | 14%  | 10%  | 11%  | 11%  | 13%  | 14%  | 14%  | 13%  | 0%   |
| [Patient_Unique_Number](#patient_unique_number)                           |                               | 100% | 100% | 99%  | 99%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 99%  | 99%  | 99%  | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Patient_Control_Number](#patient_control_number)                         |                               | 96%  | 99%  | 95%  | 99%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 99%  | 99%  | 99%  | 100% | 99%  | 99%  | 99%  | 100% |
| [Patient_Name_Last](#patient_name_last)                                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% |
| [Patient_Name_First](#patient_name_first)                                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% |
| [Patient_Name_Middle](#patient_name_middle)                               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 58%  |
| [Patient_Name_Suffix](#patient_name_suffix)                               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Patient_Street_Address](#patient_street_address)                         |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 71%  | 0%   | 100% |
| [Patient_City](#patient_city)                                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 25%  | 0%   | 100% |
| [Patient_State](#patient_state)                                           | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 97%  | 97%  | 100% |
| [Patient_County_FIPS](#patient_county_fips)                               | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Patient_Zip_Code](#patient_zip_code)                                     | X                             | 98%  | 97%  | 95%  | 98%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 97%  |
| [Patient_County](#patient_county)                                         |                               | 100% | 97%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 0%   | 0%   | 100% | 0%   | 91%  | 91%  | 60%  |
| [Patient_Country](#patient_country)                                       | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 97%  | 96%  | 64%  |
| [Patient_Date_of_Birth](#patient_date_of_birth)                           |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Patient_Gender](#patient_gender)                                         | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Patient_Marital_Status](#patient_marital_status)                         | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 99%  |
| [Patient_Race](#patient_race)                                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% | 0%   | 95%  |
| [Patient_Ethnicity](#patient_ethnicity)                                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 96%  |
| [Patient_Race_Ethnicity](#patient_race_ethnicity)                         | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Admission_Date](#admission_date)                                         |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [LOS_Days](#los_days)                                                     | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [LOS_Hours](#los_hours)                                                   | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Admission_Hour](#admission_hour)                                         | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% | 0%   | 0%   | 95%  |
| [Admission_Minute](#admission_minute)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% | 0%   | 0%   | 90%  |
| [Admission_Type](#admission_type)                                         | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% | 100% | 100% |
| [Admission_Source](#admission_source)                                     | X                             | 96%  | 97%  | 97%  | 92%  | 87%  | 88%  | 89%  | 90%  | 89%  | 91%  | 92%  | 94%  | 95%  | 95%  | 98%  | 98%  | 98%  | 100% | 100% | 99%  | 100% | 100% | 99%  |
| [Discharge_Hour](#discharge_hour)                                         |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% | 0%   | 0%   | 79%  |
| [Discharge_Minute](#discharge_minute)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% | 0%   | 0%   | 62%  |
| [Patient_Discharge_Status](#patient_discharge_status)                     | X                             | 96%  | 97%  | 98%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Patient_Discharge_Status_Uniform](#patient_discharge_status_uniform)     |                               |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Statement_Beginning_Date](#statement_beginning_date)                     |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 98%  | 100% | 100% | 100% | 100% |
| [Statement_Through_Date](#statement_through_date)                         |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Condition_Code_1](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_2](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_3](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_4](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_5](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_6](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_7](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_8](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_9](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_10](#condition_code)                                      |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_11](#condition_code)                                      |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Total_Charge_Header](#total_charge_header)                               | X                             | 99%  | 99%  | 100% | 100% | 100% | 97%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  |
| [Payer_Primary_Name](#payer_name)                                         | X                             | 98%  | 99%  | 99%  | 99%  | 99%  | 97%  | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 99%  |
| [Payer_Primary_ID](#payer_id)                                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 69%  |
| [Payer_Primary_Typology](#payer_typology)                                 | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 99%  |
| [Payer_Primary_Typology_Imputed](#payer_typology_imputed)                 | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Estimated_Amount_Due_Primary](#estimated_amount_due)                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 27%  |
| [Prior_Payment_Primary](#prior_payment)                                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 47%  |
| [Payer_Secondary_Name](#payer_name)                                       |                               | 98%  | 99%  | 99%  | 99%  | 99%  | 97%  | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 99%  |
| [Payer_Secondary_ID](#payer_id)                                           |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 69%  |
| [Payer_Secondary_Typology](#payer_typology)                               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 99%  |
| [Payer_Secondary_Typology_Imputed](#payer_typology_imputed)               |                               |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Estimated_Amount_Due_Secondary](#estimated_amount_due)                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 13%  |
| [Prior_Payment_Secondary](#prior_payment)                                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 17%  |
| [Payer_Tertiary_Name](#payer_name)                                        |                               | 98%  | 99%  | 99%  | 99%  | 99%  | 97%  | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 99%  |
| [Payer_Tertiary_ID](#payer_id)                                            |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 69%  |
| [Payer_Tertiary_Typology](#payer_typology)                                |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 99%  |
| [Payer_Tertiary_Typology_Imputed](#payer_typology_imputed)                |                               |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Estimated_Amount_Due_Tertiary](#estimated_amount_due)                    |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Prior_Payment_Tertiary](#prior_payment)                                  |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Insured_Primary_Unique_ID](#insured_unique_id)                           |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 97%  |
| [Insured_Primary_Name_Last](#insured_name_last)                           |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Insured_Primary_Name_First](#insured_name_first)                         |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Insured_Primary_Name_Middle](#insured_name_middle)                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Insured_Primary_Name_Suffix](#insured_name_suffix)                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Insured_Primary_Group_Name](#insured_group_name)                         |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Patient_Relationship_Primary](#patient_relationship)                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Insured_Secondary_Unique_ID](#insured_unique_id)                         |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 97%  |
| [Insured_Secondary_Name_Last](#insured_name_last)                         |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Insured_Secondary_Name_First](#insured_name_first)                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Insured_Secondary_Name_Middle](#insured_name_middle)                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Insured_Secondary_Name_Suffix](#insured_name_suffix)                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Insured_Secondary_Group_Name](#insured_group_name)                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Patient_Relationship_Secondary](#patient_relationship)                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Insured_Tertiary_Unique_ID](#insured_unique_id)                          |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 97%  |
| [Insured_Tertiary_Name_Last](#insured_name_last)                          |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Insured_Tertiary_Name_First](#insured_name_first)                        |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Insured_Tertiary_Name_Middle](#insured_name_middle)                      |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Insured_Tertiary_Name_Suffix](#insured_name_suffix)                      |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Insured_Tertiary_Group_Name](#insured_group_name)                        |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Patient_Relationship_Tertiary](#patient_relationship)                    |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Diagnosis_Version_Flag](#diagnosis_version_flag)                         | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Principal_Diagnosis_Code](#diagnosis_code)                               | X                             | 47%  | 96%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 99%  | 100% | 100% | 100% |
| [Principal_Diagnosis_Code_POA](#diagnosis_code_poa)                       | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 5%   |
| [Secondary_Diagnosis_Code_1](#diagnosis_code)                             | X                             | 20%  | 50%  | 54%  | 56%  | 59%  | 61%  | 62%  | 62%  | 63%  | 65%  | 69%  | 71%  | 73%  | 72%  | 72%  | 71%  | 72%  | 72%  | 73%  | 73%  | 72%  | 71%  | 65%  |
| [Secondary_Diagnosis_Code_POA_1](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 3%   |
| [Secondary_Diagnosis_Code_2](#diagnosis_code)                             | X                             | 9%   | 26%  | 29%  | 33%  | 34%  | 36%  | 38%  | 38%  | 38%  | 41%  | 45%  | 49%  | 51%  | 52%  | 51%  | 49%  | 49%  | 49%  | 51%  | 51%  | 50%  | 47%  | 41%  |
| [Secondary_Diagnosis_Code_POA_2](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   |
| [Secondary_Diagnosis_Code_3](#diagnosis_code)                             | X                             | 4%   | 13%  | 14%  | 18%  | 18%  | 20%  | 22%  | 22%  | 22%  | 24%  | 28%  | 31%  | 33%  | 34%  | 33%  | 31%  | 32%  | 31%  | 34%  | 35%  | 34%  | 31%  | 26%  |
| [Secondary_Diagnosis_Code_POA_3](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   |
| [Secondary_Diagnosis_Code_4](#diagnosis_code)                             | X                             | 2%   | 6%   | 7%   | 9%   | 9%   | 11%  | 12%  | 12%  | 13%  | 14%  | 17%  | 20%  | 21%  | 23%  | 22%  | 20%  | 20%  | 20%  | 22%  | 23%  | 22%  | 20%  | 16%  |
| [Secondary_Diagnosis_Code_POA_4](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   |
| [Secondary_Diagnosis_Code_5](#diagnosis_code)                             | X                             | 1%   | 3%   | 3%   | 5%   | 4%   | 5%   | 6%   | 7%   | 7%   | 8%   | 11%  | 12%  | 14%  | 15%  | 15%  | 13%  | 13%  | 13%  | 15%  | 16%  | 15%  | 12%  | 11%  |
| [Secondary_Diagnosis_Code_POA_5](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   |
| [Secondary_Diagnosis_Code_6](#diagnosis_code)                             | X                             | 0%   | 1%   | 2%   | 2%   | 2%   | 3%   | 3%   | 4%   | 4%   | 5%   | 6%   | 7%   | 9%   | 10%  | 9%   | 8%   | 8%   | 8%   | 10%  | 10%  | 10%  | 8%   | 7%   |
| [Secondary_Diagnosis_Code_POA_6](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   |
| [Secondary_Diagnosis_Code_7](#diagnosis_code)                             | X                             | 0%   | 1%   | 1%   | 1%   | 1%   | 1%   | 2%   | 2%   | 2%   | 3%   | 4%   | 4%   | 5%   | 6%   | 6%   | 5%   | 5%   | 5%   | 7%   | 7%   | 6%   | 5%   | 5%   |
| [Secondary_Diagnosis_Code_POA_7](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_8](#diagnosis_code)                             | X                             | 0%   | 0%   | 0%   | 1%   | 0%   | 1%   | 1%   | 1%   | 1%   | 1%   | 2%   | 3%   | 3%   | 4%   | 4%   | 3%   | 3%   | 3%   | 5%   | 5%   | 4%   | 3%   | 3%   |
| [Secondary_Diagnosis_Code_POA_8](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_9](#diagnosis_code)                             | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   |
| [Secondary_Diagnosis_Code_POA_9](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_10](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   |
| [Secondary_Diagnosis_Code_POA_10](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_11](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   |
| [Secondary_Diagnosis_Code_POA_11](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_12](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   |
| [Secondary_Diagnosis_Code_POA_12](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_13](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   |
| [Secondary_Diagnosis_Code_POA_13](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_14](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_POA_14](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_15](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_POA_15](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_16](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_POA_16](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_17](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_POA_17](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Admitting_Diagnosis_Code](#diagnosis_code)                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 47%  |
| [Patient_Reason_For_Visit_1](#patient_reason_for_visit)                   | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 16%  | 51%  | 51%  | 48%  |
| [Patient_Reason_For_Visit_2](#patient_reason_for_visit)                   | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   | 6%   | 5%   | 10%  |
| [Patient_Reason_For_Visit_3](#patient_reason_for_visit)                   | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 2%   | 2%   | 5%   |
| [E_Code_1](#e_code)                                                       | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 6%   |
| [E_Code_POA_1](#e_code_poa)                                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [E_Code_2](#e_code)                                                       | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   |
| [E_Code_POA_2](#e_code_poa)                                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [E_Code_3](#e_code)                                                       | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   |
| [E_Code_POA_3](#e_code_poa)                                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Principal_ICD_Procedure](#icd_procedure)                                 | X                             | 64%  | 78%  | 76%  | 79%  | 80%  | 78%  | 77%  | 77%  | 77%  | 83%  | 84%  | 85%  | 82%  | 80%  | 72%  | 74%  | 74%  | 66%  | 59%  | 42%  | 0%   | 0%   | 3%   |
| [Procedure_Date_Principal](#procedure_date)                               |                               | 100% | 100% | 100% | 100% | 98%  | 97%  | 97%  | 97%  | 97%  | 98%  | 98%  | 97%  | 98%  | 99%  | 100% | 100% | 97%  | 100% | 91%  | 92%  | 90%  | 99%  | 3%   |
| [Secondary_ICD_Procedure_1](#icd_procedure)                               | X                             | 33%  | 39%  | 37%  | 39%  | 37%  | 37%  | 36%  | 36%  | 35%  | 36%  | 37%  | 37%  | 37%  | 36%  | 31%  | 32%  | 32%  | 28%  | 25%  | 18%  | 0%   | 0%   | 1%   |
| [Procedure_Date_Secondary_1](#procedure_date)                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   |
| [Secondary_ICD_Procedure_2](#icd_procedure)                               | X                             | 11%  | 13%  | 12%  | 13%  | 12%  | 14%  | 14%  | 14%  | 14%  | 15%  | 15%  | 15%  | 15%  | 14%  | 12%  | 12%  | 12%  | 11%  | 10%  | 7%   | 0%   | 0%   | 0%   |
| [Procedure_Date_Secondary_2](#procedure_date)                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   |
| [Secondary_ICD_Procedure_3](#icd_procedure)                               | X                             | 7%   | 4%   | 4%   | 4%   | 4%   | 5%   | 4%   | 5%   | 4%   | 4%   | 5%   | 5%   | 5%   | 5%   | 4%   | 5%   | 5%   | 4%   | 4%   | 3%   | 0%   | 0%   | 0%   |
| [Procedure_Date_Secondary_3](#procedure_date)                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   |
| [Secondary_ICD_Procedure_4](#icd_procedure)                               | X                             | 3%   | 2%   | 2%   | 2%   | 1%   | 2%   | 2%   | 2%   | 1%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 1%   | 0%   | 0%   | 0%   |
| [Procedure_Date_Secondary_4](#procedure_date)                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_ICD_Procedure_5](#icd_procedure)                               | X                             | 2%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 0%   | 0%   | 0%   |
| [Procedure_Date_Secondary_5](#procedure_date)                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Diagnosis_Related_Group](#diagnosis_related_group)                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Attending_ID](#provider_id)                                     |                               | 77%  | 96%  | 94%  | 91%  | 93%  | 89%  | 92%  | 93%  | 90%  | 95%  | 95%  | 95%  | 95%  | 94%  | 97%  | 94%  | 94%  | 91%  | 86%  | 0%   | 71%  | 71%  | 35%  |
| [Provider_Attending_NPI](#provider_npi)                                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 0%   | 0%   | 86%  | 85%  | 0%   | 0%   | 98%  |
| [Provider_Attending_ID_Qualifier](#provider_id_qualifier)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 52%  |
| [Provider_Attending_Taxonomy_Code](#provider_taxonomy_code)               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Attending_AMA_Specialty](#provider_ama_specialty)               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Attending_Other_Type_Qualifier](#provider_other_type_qualifier) |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Operating_ID](#provider_id)                                     |                               | 8%   | 18%  | 15%  | 11%  | 11%  | 11%  | 32%  | 32%  | 32%  | 37%  | 38%  | 41%  | 43%  | 43%  | 51%  | 52%  | 65%  | 60%  | 70%  | 0%   | 50%  | 63%  | 23%  |
| [Provider_Operating_NPI](#provider_npi)                                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   | 0%   | 0%   | 70%  | 73%  | 0%   | 0%   | 39%  |
| [Provider_Operating_ID_Qualifier](#provider_id_qualifier)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 27%  |
| [Provider_Operating_Taxonomy_Code](#provider_taxonomy_code)               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Operating_AMA_Specialty](#provider_ama_specialty)               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Operating_Other_Type_Qualifier](#provider_other_type_qualifier) |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_1_ID](#provider_id)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 16%  |
| [Provider_Other_1_NPI](#provider_npi)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 66%  |
| [Provider_Other_1_ID_Qualifier](#provider_qualifier)                      |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 76%  |
| [Provider_Other_1_Taxonomy_Code](#provider_taxonomy_code)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_1_AMA_Specialty](#provider_ama_specialty)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_1_Other_Type_Qualifier](#provider_other_type_qualifier)   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 32%  |
| [Provider_Other_2_ID](#provider_id)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 3%   |
| [Provider_Other_2_NPI](#provider_npi)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 8%   |
| [Provider_Other_2_ID_Qualifier](#provider_id_qualifier)                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 39%  |
| [Provider_Other_2_Taxonomy_Code](#provider_taxonomy_code)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_2_AMA_Specialty](#provider_ama_specialty)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_2_Other_Type_Qualifier](#provider_other_type_qualifier)   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 3%   |
| [Provider_Other_3_ID](#provider_id)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_3_NPI](#provider_npi)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_3_ID_Qualifier](#provider_id_qualifier)                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_3_Taxonomy_Code](#provider_taxonomy_code)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_3_AMA_Specialty](#provider_ama_specialty)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_3_Other_Type_Qualifier](#provider_other_type_qualifier)   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_ID](#provider_id)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_NPI](#provider_npi)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_ID_Qualifier](#provider_id_qualifier)                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_Taxonomy_Code](#provider_taxonomy_code)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_AMA_Specialty](#provider_ama_specialty)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_Other_Type_Qualifier](#provider_other_type_qualifier)   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Data_Source](#data_source)                                               |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 0%   | 0%   | 100% | 100% |
| [Service_Line](#service_line)                                             | X (AS only)                   | 46%  | 29%  | 29%  | 26%  | 29%  | 29%  | 42%  | 47%  | 51%  | 48%  | 45%  | 46%  | 49%  | 60%  | 56%  | 58%  | 48%  | 39%  | 41%  | 41%  | 45%  | 62%  | 82%  |
| [Revenue_Code](#revenue_code)                                             | X (AS only)                   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 99%  |
| [HCPCS_Code](#hcpcs_code)                                                 | X (AS only)                   | 36%  | 88%  | 88%  | 83%  | 87%  | 84%  | 97%  | 98%  | 99%  | 98%  | 98%  | 99%  | 99%  | 93%  | 92%  | 97%  | 96%  | 94%  | 91%  | 94%  | 98%  | 99%  | 99%  |
| [HCPCS_Code_Mod1](#hcpcs_code_mod)                                        | X (AS only)                   | 70%  | 8%   | 27%  | 13%  | 24%  | 32%  | 35%  | 36%  | 36%  | 36%  | 35%  | 36%  | 35%  | 36%  | 33%  | 35%  | 38%  | 38%  | 37%  | 36%  | 37%  | 38%  | 45%  |
| [HCPCS_Code_Mod2](#hcpcs_code_mod)                                        | X (AS only)                   | 65%  | 1%   | 4%   | 3%   | 5%   | 6%   | 7%   | 8%   | 9%   | 9%   | 9%   | 9%   | 8%   | 8%   | 7%   | 8%   | 6%   | 6%   | 32%  | 4%   | 4%   | 4%   | 9%   |
| [HCPCS_Code_Mod3](#hcpcs_code_mod)                                        |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [HCPCS_Code_Mod4](#hcpcs_code_mod)                                        |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [National_Drug_Code](#national_drug_code)                                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 34%  |
| [Service_Line_Date](#service_line_date)                                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 99%  |
| [Service_Units](#service_units)                                           |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 99%  |
| [Unit_Of_Measurement](#unit_of_measurement)                               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 82%  |
| [Total_Charge_By_Revenue_Code](#total_charge_by_revenue_code)             |                               | 46%  | 29%  | 29%  | 26%  | 29%  | 29%  | 42%  | 47%  | 51%  | 48%  | 45%  | 46%  | 49%  | 60%  | 56%  | 58%  | 48%  | 39%  | 41%  | 41%  | 45%  | 62%  | 82%  |
| [ER_Charges](#er_charges)                                                 | X                             | 46%  | 29%  | 29%  | 26%  | 29%  | 29%  | 42%  | 47%  | 51%  | 48%  | 45%  | 46%  | 49%  | 60%  | 56%  | 58%  | 48%  | 39%  | 41%  | 41%  | 45%  | 62%  | 82%  |
| [Facility_Charges](#facility_charges)                                     | X                             | 46%  | 29%  | 29%  | 26%  | 29%  | 29%  | 42%  | 47%  | 51%  | 48%  | 45%  | 46%  | 49%  | 60%  | 56%  | 58%  | 48%  | 39%  | 41%  | 41%  | 45%  | 62%  | 82%  |
| [Professional_Charges](#professional_charges)                             | X                             | 46%  | 29%  | 29%  | 26%  | 29%  | 29%  | 42%  | 47%  | 51%  | 48%  | 45%  | 46%  | 49%  | 60%  | 56%  | 58%  | 48%  | 39%  | 41%  | 41%  | 45%  | 62%  | 82%  |
| [MSGMCE_Version_Used](#msgmce_version_used)                               | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Initial_DRG](#initial_drg)                                               |                               | 0%   | 0%   | 26%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 99%  | 99%  | 100% | 100% | 100% |
| [Initial_MS_Indicator](#initial_ms_indicator)                             |                               | 0%   | 0%   | 26%  | 98%  | 99%  | 99%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 99%  | 99%  | 99%  | 99%  | 99%  |
| [Final_MDC](#final_mdc)                                                   | X                             | 0%   | 0%   | 26%  | 98%  | 99%  | 99%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 99%  | 99%  | 100% | 100% | 100% |
| [Final_DRG](#final_drg)                                                   | X                             | 0%   | 0%   | 26%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 99%  | 99%  | 100% | 100% | 100% |
| [Final_MS_Indicator](#final_ms_indicator)                                 | X                             | 0%   | 0%   | 26%  | 98%  | 99%  | 99%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 99%  | 99%  | 99%  | 99%  | 99%  |
| [DRG_Return_Code](#drg_return_code)                                       | X                             | 100% | 100% | 74%  | 2%   | 1%   | 1%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 1%   | 0%   | 0%   | 0%   |
| [MSGMCE_Edit_Return_Code](#msgmce_edit_return_code)                       | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Initial_4digit_DRG](#initial_4digit_drg)                                 | X                             | 0%   | 0%   | 26%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 99%  | 99%  | 100% | 100% | 100% |
| [Final_4digit_DRG](#final_4digit_drg)                                     | X                             | 0%   | 0%   | 26%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 99%  | 99%  | 100% | 100% | 100% |
| [Final_DRG_CC_MCC_Usage](#final_drg_cc_mcc_usage)                         | X                             | 0%   | 0%   | 1%   | 4%   | 4%   | 4%   | 4%   | 4%   | 4%   | 4%   | 4%   | 4%   | 5%   | 5%   | 5%   | 5%   | 5%   | 6%   | 6%   | 6%   | 6%   | 7%   | 6%   |
| [Initial_DRG_CC_MCC_Usage](#initial_drg_cc_mcc_usage)                     | X                             | 0%   | 0%   | 1%   | 4%   | 4%   | 4%   | 4%   | 4%   | 4%   | 4%   | 4%   | 4%   | 5%   | 5%   | 5%   | 5%   | 5%   | 6%   | 6%   | 6%   | 6%   | 7%   | 6%   |
| [Hospital_Acquired_Conditions](#hospital_acquired_conditions)             | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Hospital_Acquired_Condition_Status](#hospital_acquired_condition_status) |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Cost_Weight](#cost_weight)                                               | X                             | 0%   | 0%   | 26%  | 98%  | 99%  | 99%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 99%  | 99%  | 99%  | 99%  | 99%  |

### Emergency Department

|                               Variable Name                               | Standard Limited-Use Variable | 1996 | 1997 | 1998 | 1999 | 2000 | 2001 | 2002 | 2003 | 2004 | 2005 | 2006 | 2007 | 2008 | 2009 | 2010 | 2011 | 2012 | 2013 | 2014 | 2015 | 2016 | 2017 | 2018 |
| ------------------------------------------------------------------------- | ----------------------------- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| [Record_ID](#record_id)                                                   | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Encounter_Type](#encounter_type)                                         | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Facility_Number](#facility_number)                                       | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Facility_Name](#facility_name)                                           | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Age_Group_Num](#age_group_num)                                           | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Age_Group](#age_group)                                                   | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Bill_Type](#bill_type)                                                   |                               | 100% | 100% | 100% | 99%  | 77%  | 33%  | 32%  | 32%  | 32%  | 32%  | 33%  | 32%  | 32%  | 31%  | 31%  | 31%  | 31%  | 31%  | 31%  | 35%  | 53%  | 90%  | 100% |
| [ER_Flag](#er_flag)                                                       | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Observation_Stay_Flag](#observation_stay_flag)                           | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Year](#year)                                                             | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Quarter](#quarter)                                                       | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Patient_Medical_Record_Number](#patient_medical_record_number)           |                               | 100% | 99%  | 99%  | 91%  | 99%  | 89%  | 88%  | 88%  | 90%  | 90%  | 91%  | 91%  | 88%  | 88%  | 90%  | 90%  | 91%  | 91%  | 92%  | 92%  | 92%  | 97%  | 100% |
| [Patient_Social_Security_Number](#patient_social_security_number)         |                               | 13%  | 6%   | 11%  | 80%  | 80%  | 78%  | 80%  | 79%  | 79%  | 79%  | 81%  | 81%  | 79%  | 77%  | 79%  | 80%  | 80%  | 80%  | 80%  | 83%  | 80%  | 80%  | 80%  |
| [Patient_Social_Security_Flag](#patient_social_security_flag)             |                               | 1%   | 0%   | 0%   | 18%  | 17%  | 21%  | 19%  | 20%  | 19%  | 19%  | 18%  | 18%  | 19%  | 22%  | 20%  | 20%  | 19%  | 19%  | 19%  | 17%  | 16%  | 14%  | 0%   |
| [Patient_Unique_Number](#patient_unique_number)                           |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 0%   |
| [Patient_Control_Number](#patient_control_number)                         |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Patient_Name_Last](#patient_name_last)                                   |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Patient_Name_First](#patient_name_first)                                 |                               | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Patient_Name_Middle](#patient_name_middle)                               |                               | 34%  | 36%  | 40%  | 42%  | 39%  | 56%  | 60%  | 60%  | 61%  | 63%  | 64%  | 64%  | 64%  | 64%  | 66%  | 68%  | 68%  | 68%  | 68%  | 68%  | 66%  | 66%  | 64%  |
| [Patient_Name_Suffix](#patient_name_suffix)                               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   |
| [Patient_Street_Address](#patient_street_address)                         |                               | 14%  | 22%  | 24%  | 25%  | 23%  | 11%  | 10%  | 9%   | 10%  | 7%   | 8%   | 9%   | 9%   | 8%   | 8%   | 6%   | 6%   | 11%  | 24%  | 35%  | 66%  | 90%  | 99%  |
| [Patient_City](#patient_city)                                             |                               | 14%  | 22%  | 23%  | 24%  | 23%  | 11%  | 10%  | 9%   | 10%  | 7%   | 8%   | 9%   | 9%   | 8%   | 8%   | 6%   | 6%   | 11%  | 24%  | 35%  | 66%  | 90%  | 100% |
| [Patient_State](#patient_state)                                           | X                             | 14%  | 23%  | 24%  | 25%  | 23%  | 11%  | 10%  | 10%  | 10%  | 7%   | 8%   | 9%   | 9%   | 8%   | 8%   | 6%   | 7%   | 11%  | 24%  | 35%  | 98%  | 99%  | 100% |
| [Patient_County_FIPS](#patient_county_fips)                               | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Patient_Zip_Code](#patient_zip_code)                                     | X                             | 99%  | 99%  | 99%  | 99%  | 99%  | 100% | 100% | 99%  | 99%  | 97%  | 97%  | 98%  | 98%  | 98%  | 98%  | 98%  | 98%  | 98%  | 98%  | 100% | 100% | 100% | 97%  |
| [Patient_County](#patient_county)                                         |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 0%   | 92%  | 92%  | 84%  |
| [Patient_Country](#patient_country)                                       | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 14%  | 99%  | 99%  | 74%  |
| [Patient_Date_of_Birth](#patient_date_of_birth)                           |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Patient_Gender](#patient_gender)                                         | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Patient_Marital_Status](#patient_marital_status)                         | X                             | 86%  | 95%  | 95%  | 92%  | 92%  | 45%  | 44%  | 44%  | 44%  | 43%  | 44%  | 42%  | 40%  | 39%  | 39%  | 58%  | 99%  | 98%  | 99%  | 100% | 99%  | 98%  | 100% |
| [Patient_Race](#patient_race)                                             |                               | 85%  | 84%  | 80%  | 88%  | 87%  | 43%  | 42%  | 41%  | 41%  | 41%  | 92%  | 93%  | 94%  | 95%  | 96%  | 96%  | 95%  | 94%  | 96%  | 98%  | 100% | 97%  | 99%  |
| [Patient_Ethnicity](#patient_ethnicity)                                   |                               | 85%  | 84%  | 80%  | 87%  | 67%  | 28%  | 28%  | 27%  | 28%  | 29%  | 80%  | 80%  | 83%  | 85%  | 88%  | 90%  | 88%  | 88%  | 91%  | 100% | 100% | 73%  | 99%  |
| [Patient_Race_Ethnicity](#patient_race_ethnicity)                         | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Admission_Date](#admission_date)                                         |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [LOS_Days](#los_days)                                                     | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [LOS_Hours](#los_hours)                                                   | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Admission_Hour](#admission_hour)                                         | X                             | 8%   | 28%  | 92%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 96%  | 96%  | 97%  | 96%  | 95%  | 96%  | 96%  | 96%  | 97%  | 97%  |
| [Admission_Minute](#admission_minute)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 98%  | 98%  | 98%  | 93%  |
| [Admission_Type](#admission_type)                                         | X                             | 100% | 100% | 100% | 100% | 100% | 49%  | 48%  | 48%  | 48%  | 48%  | 48%  | 47%  | 46%  | 45%  | 43%  | 44%  | 44%  | 44%  | 45%  | 100% | 100% | 100% | 100% |
| [Admission_Source](#admission_source)                                     | X                             | 14%  | 19%  | 92%  | 100% | 100% | 49%  | 48%  | 48%  | 48%  | 48%  | 48%  | 47%  | 46%  | 45%  | 43%  | 44%  | 44%  | 44%  | 45%  | 100% | 100% | 100% | 100% |
| [Discharge_Hour](#discharge_hour)                                         |                               | 8%   | 9%   | 76%  | 94%  | 93%  | 95%  | 95%  | 96%  | 97%  | 96%  | 97%  | 96%  | 97%  | 95%  | 95%  | 96%  | 95%  | 93%  | 94%  | 47%  | 64%  | 86%  | 95%  |
| [Discharge_Minute](#discharge_minute)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 44%  | 48%  | 71%  | 76%  |
| [Patient_Discharge_Status](#patient_discharge_status)                     | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Patient_Discharge_Status_Uniform](#patient_discharge_status_uniform)     |                               |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Statement_Beginning_Date](#statement_beginning_date)                     |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Statement_Through_Date](#statement_through_date)                         |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Condition_Code_1](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 28%  | 51%  | 0%   |
| [Condition_Code_2](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_3](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_4](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_5](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_6](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_7](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_8](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_9](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_10](#condition_code)                                      |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_11](#condition_code)                                      |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Total_Charge_Header](#total_charge_header)                               | X                             | 95%  | 98%  | 93%  | 97%  | 98%  | 99%  | 99%  | 99%  | 99%  | 99%  | 99%  | 99%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Payer_Primary_Name](#payer_name)                                         | X                             | 77%  | 83%  | 97%  | 100% | 96%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 96%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 98%  | 97%  | 99%  |
| [Payer_Primary_ID](#payer_id)                                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 67%  |
| [Payer_Primary_Typology](#payer_typology)                                 | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% |
| [Payer_Primary_Typology_Imputed](#payer_typology_imputed)                 | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Estimated_Amount_Due_Primary](#estimated_amount_due)                     |                               | 0%   | 0%   | 0%   | 7%   | 8%   | 6%   | 4%   | 5%   | 5%   | 4%   | 5%   | 6%   | 5%   | 4%   | 4%   | 4%   | 3%   | 2%   | 3%   | 17%  | 23%  | 38%  | 40%  |
| [Prior_Payment_Primary](#prior_payment)                                   |                               | 0%   | 0%   | 0%   | 1%   | 18%  | 12%  | 13%  | 12%  | 13%  | 14%  | 14%  | 12%  | 13%  | 12%  | 11%  | 11%  | 10%  | 10%  | 11%  | 49%  | 49%  | 62%  | 62%  |
| [Payer_Secondary_Name](#payer_name)                                       |                               | 77%  | 83%  | 97%  | 100% | 96%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 96%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 98%  | 97%  | 99%  |
| [Payer_Secondary_ID](#payer_id)                                           |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 67%  |
| [Payer_Secondary_Typology](#payer_typology)                               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% |
| [Payer_Secondary_Typology_Imputed](#payer_typology_imputed)               |                               |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Estimated_Amount_Due_Secondary](#estimated_amount_due)                   |                               | 0%   | 0%   | 0%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 0%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 0%   | 1%   | 2%   | 7%   | 17%  | 25%  |
| [Prior_Payment_Secondary](#prior_payment)                                 |                               | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 1%   | 1%   | 1%   | 2%   | 2%   | 1%   | 2%   | 1%   | 1%   | 1%   | 1%   | 1%   | 2%   | 11%  | 13%  | 19%  | 23%  |
| [Payer_Tertiary_Name](#payer_name)                                        |                               | 77%  | 83%  | 97%  | 100% | 96%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 96%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 98%  | 97%  | 99%  |
| [Payer_Tertiary_ID](#payer_id)                                            |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 67%  |
| [Payer_Tertiary_Typology](#payer_typology)                                |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% |
| [Payer_Tertiary_Typology_Imputed](#payer_typology_imputed)                |                               |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Estimated_Amount_Due_Tertiary](#estimated_amount_due)                    |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 1%   |
| [Prior_Payment_Tertiary](#prior_payment)                                  |                               | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   | 1%   | 1%   | 1%   |
| [Insured_Primary_Unique_ID](#insured_unique_id)                           |                               | 11%  | 18%  | 82%  | 87%  | 89%  | 44%  | 43%  | 43%  | 44%  | 43%  | 44%  | 42%  | 41%  | 40%  | 39%  | 39%  | 37%  | 39%  | 40%  | 14%  | 60%  | 79%  | 88%  |
| [Insured_Primary_Name_Last](#insured_name_last)                           |                               | 11%  | 39%  | 86%  | 91%  | 92%  | 45%  | 44%  | 44%  | 44%  | 44%  | 44%  | 43%  | 42%  | 40%  | 38%  | 37%  | 35%  | 35%  | 40%  | 45%  | 61%  | 82%  | 0%   |
| [Insured_Primary_Name_First](#insured_name_first)                         |                               | 11%  | 39%  | 86%  | 91%  | 92%  | 45%  | 44%  | 44%  | 44%  | 44%  | 44%  | 43%  | 42%  | 40%  | 38%  | 37%  | 35%  | 35%  | 40%  | 45%  | 61%  | 82%  | 0%   |
| [Insured_Primary_Name_Middle](#insured_name_middle)                       |                               | 3%   | 25%  | 34%  | 37%  | 36%  | 18%  | 18%  | 19%  | 21%  | 23%  | 24%  | 24%  | 23%  | 22%  | 22%  | 23%  | 22%  | 22%  | 24%  | 27%  | 37%  | 53%  | 0%   |
| [Insured_Primary_Name_Suffix](#insured_name_suffix)                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   |
| [Insured_Primary_Group_Name](#insured_group_name)                         |                               | 3%   | 2%   | 11%  | 15%  | 18%  | 9%   | 9%   | 9%   | 8%   | 7%   | 7%   | 8%   | 8%   | 6%   | 5%   | 7%   | 7%   | 9%   | 10%  | 11%  | 10%  | 11%  | 0%   |
| [Patient_Relationship_Primary](#patient_relationship)                     |                               | 10%  | 70%  | 82%  | 88%  | 90%  | 45%  | 81%  | 94%  | 92%  | 94%  | 94%  | 94%  | 94%  | 94%  | 94%  | 95%  | 96%  | 95%  | 95%  | 89%  | 95%  | 93%  | 0%   |
| [Insured_Secondary_Unique_ID](#insured_unique_id)                         |                               | 11%  | 18%  | 82%  | 87%  | 89%  | 44%  | 43%  | 43%  | 44%  | 43%  | 44%  | 42%  | 41%  | 40%  | 39%  | 39%  | 37%  | 39%  | 40%  | 14%  | 60%  | 79%  | 88%  |
| [Insured_Secondary_Name_Last](#insured_name_last)                         |                               | 11%  | 39%  | 86%  | 91%  | 92%  | 45%  | 44%  | 44%  | 44%  | 44%  | 44%  | 43%  | 42%  | 40%  | 38%  | 37%  | 35%  | 35%  | 40%  | 45%  | 61%  | 82%  | 0%   |
| [Insured_Secondary_Name_First](#insured_name_first)                       |                               | 11%  | 39%  | 86%  | 91%  | 92%  | 45%  | 44%  | 44%  | 44%  | 44%  | 44%  | 43%  | 42%  | 40%  | 38%  | 37%  | 35%  | 35%  | 40%  | 45%  | 61%  | 82%  | 0%   |
| [Insured_Secondary_Name_Middle](#insured_name_middle)                     |                               | 3%   | 25%  | 34%  | 37%  | 36%  | 18%  | 18%  | 19%  | 21%  | 23%  | 24%  | 24%  | 23%  | 22%  | 22%  | 23%  | 22%  | 22%  | 24%  | 27%  | 37%  | 53%  | 0%   |
| [Insured_Secondary_Name_Suffix](#insured_name_suffix)                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   |
| [Insured_Secondary_Group_Name](#insured_group_name)                       |                               | 3%   | 2%   | 11%  | 15%  | 18%  | 9%   | 9%   | 9%   | 8%   | 7%   | 7%   | 8%   | 8%   | 6%   | 5%   | 7%   | 7%   | 9%   | 10%  | 11%  | 10%  | 11%  | 0%   |
| [Patient_Relationship_Secondary](#patient_relationship)                   |                               | 2%   | 67%  | 35%  | 37%  | 37%  | 16%  | 25%  | 32%  | 33%  | 37%  | 39%  | 41%  | 42%  | 44%  | 42%  | 46%  | 46%  | 46%  | 43%  | 62%  | 48%  | 30%  | 0%   |
| [Insured_Tertiary_Unique_ID](#insured_unique_id)                          |                               | 11%  | 18%  | 82%  | 87%  | 89%  | 44%  | 43%  | 43%  | 44%  | 43%  | 44%  | 42%  | 41%  | 40%  | 39%  | 39%  | 37%  | 39%  | 40%  | 14%  | 60%  | 79%  | 88%  |
| [Insured_Tertiary_Name_Last](#insured_name_last)                          |                               | 11%  | 39%  | 86%  | 91%  | 92%  | 45%  | 44%  | 44%  | 44%  | 44%  | 44%  | 43%  | 42%  | 40%  | 38%  | 37%  | 35%  | 35%  | 40%  | 45%  | 61%  | 82%  | 0%   |
| [Insured_Tertiary_Name_First](#insured_name_first)                        |                               | 11%  | 39%  | 86%  | 91%  | 92%  | 45%  | 44%  | 44%  | 44%  | 44%  | 44%  | 43%  | 42%  | 40%  | 38%  | 37%  | 35%  | 35%  | 40%  | 45%  | 61%  | 82%  | 0%   |
| [Insured_Tertiary_Name_Middle](#insured_name_middle)                      |                               | 3%   | 25%  | 34%  | 37%  | 36%  | 18%  | 18%  | 19%  | 21%  | 23%  | 24%  | 24%  | 23%  | 22%  | 22%  | 23%  | 22%  | 22%  | 24%  | 27%  | 37%  | 53%  | 0%   |
| [Insured_Tertiary_Name_Suffix](#insured_name_suffix)                      |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   |
| [Insured_Tertiary_Group_Name](#insured_group_name)                        |                               | 3%   | 2%   | 11%  | 15%  | 18%  | 9%   | 9%   | 9%   | 8%   | 7%   | 7%   | 8%   | 8%   | 6%   | 5%   | 7%   | 7%   | 9%   | 10%  | 11%  | 10%  | 11%  | 0%   |
| [Patient_Relationship_Tertiary](#patient_relationship)                    |                               | 0%   | 66%  | 14%  | 15%  | 14%  | 5%   | 6%   | 10%  | 11%  | 11%  | 14%  | 15%  | 15%  | 16%  | 16%  | 20%  | 21%  | 19%  | 13%  | 52%  | 35%  | 12%  | 0%   |
| [Diagnosis_Version_Flag](#diagnosis_version_flag)                         | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Principal_Diagnosis_Code](#diagnosis_code)                               | X                             | 99%  | 98%  | 99%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |
| [Principal_Diagnosis_Code_POA](#diagnosis_code_poa)                       | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% | 100% | 2%   |
| [Secondary_Diagnosis_Code_1](#diagnosis_code)                             | X                             | 40%  | 44%  | 46%  | 52%  | 53%  | 63%  | 64%  | 66%  | 67%  | 68%  | 70%  | 71%  | 70%  | 71%  | 75%  | 75%  | 76%  | 75%  | 77%  | 77%  | 75%  | 73%  | 79%  |
| [Secondary_Diagnosis_Code_POA_1](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 75%  | 73%  | 2%   |
| [Secondary_Diagnosis_Code_2](#diagnosis_code)                             | X                             | 13%  | 16%  | 20%  | 24%  | 21%  | 38%  | 39%  | 41%  | 42%  | 43%  | 45%  | 46%  | 47%  | 47%  | 52%  | 51%  | 52%  | 51%  | 53%  | 54%  | 49%  | 47%  | 56%  |
| [Secondary_Diagnosis_Code_POA_2](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 49%  | 47%  | 1%   |
| [Secondary_Diagnosis_Code_3](#diagnosis_code)                             | X                             | 4%   | 5%   | 7%   | 8%   | 7%   | 15%  | 17%  | 19%  | 21%  | 22%  | 24%  | 25%  | 26%  | 27%  | 34%  | 33%  | 32%  | 33%  | 34%  | 35%  | 31%  | 29%  | 38%  |
| [Secondary_Diagnosis_Code_POA_3](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 31%  | 29%  | 1%   |
| [Secondary_Diagnosis_Code_4](#diagnosis_code)                             | X                             | 1%   | 2%   | 2%   | 3%   | 2%   | 6%   | 8%   | 9%   | 11%  | 12%  | 14%  | 14%  | 14%  | 16%  | 21%  | 20%  | 19%  | 20%  | 21%  | 21%  | 19%  | 18%  | 23%  |
| [Secondary_Diagnosis_Code_POA_4](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 19%  | 18%  | 1%   |
| [Secondary_Diagnosis_Code_5](#diagnosis_code)                             | X                             | 1%   | 1%   | 1%   | 1%   | 1%   | 3%   | 4%   | 5%   | 6%   | 7%   | 8%   | 8%   | 8%   | 9%   | 12%  | 11%  | 10%  | 11%  | 12%  | 13%  | 11%  | 11%  | 15%  |
| [Secondary_Diagnosis_Code_POA_5](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 11%  | 11%  | 0%   |
| [Secondary_Diagnosis_Code_6](#diagnosis_code)                             | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 2%   | 3%   | 4%   | 4%   | 5%   | 5%   | 5%   | 5%   | 8%   | 6%   | 6%   | 6%   | 7%   | 7%   | 7%   | 7%   | 9%   |
| [Secondary_Diagnosis_Code_POA_6](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 7%   | 7%   | 0%   |
| [Secondary_Diagnosis_Code_7](#diagnosis_code)                             | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 1%   | 2%   | 2%   | 3%   | 3%   | 3%   | 3%   | 5%   | 4%   | 3%   | 4%   | 4%   | 5%   | 4%   | 5%   | 6%   |
| [Secondary_Diagnosis_Code_POA_7](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 4%   | 5%   | 0%   |
| [Secondary_Diagnosis_Code_8](#diagnosis_code)                             | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 1%   | 2%   | 2%   | 2%   | 2%   | 3%   | 2%   | 2%   | 2%   | 3%   | 3%   | 3%   | 3%   | 4%   |
| [Secondary_Diagnosis_Code_POA_8](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 3%   | 3%   | 0%   |
| [Secondary_Diagnosis_Code_9](#diagnosis_code)                             | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 2%   | 2%   | 3%   |
| [Secondary_Diagnosis_Code_POA_9](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   | 2%   | 0%   |
| [Secondary_Diagnosis_Code_10](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 2%   | 2%   |
| [Secondary_Diagnosis_Code_POA_10](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 2%   | 0%   |
| [Secondary_Diagnosis_Code_11](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 2%   |
| [Secondary_Diagnosis_Code_POA_11](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 0%   |
| [Secondary_Diagnosis_Code_12](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   |
| [Secondary_Diagnosis_Code_POA_12](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   |
| [Secondary_Diagnosis_Code_13](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   |
| [Secondary_Diagnosis_Code_POA_13](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   |
| [Secondary_Diagnosis_Code_14](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   |
| [Secondary_Diagnosis_Code_POA_14](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_15](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_POA_15](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_16](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_POA_16](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_17](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_POA_17](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Admitting_Diagnosis_Code](#diagnosis_code)                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 59%  | 64%  | 55%  | 76%  |
| [Patient_Reason_For_Visit_1](#patient_reason_for_visit)                   | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 62%  | 80%  | 79%  | 80%  |
| [Patient_Reason_For_Visit_2](#patient_reason_for_visit)                   | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 11%  | 14%  | 17%  | 17%  |
| [Patient_Reason_For_Visit_3](#patient_reason_for_visit)                   | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 4%   | 5%   | 8%   | 7%   |
| [E_Code_1](#e_code)                                                       | X                             | 38%  | 34%  | 37%  | 38%  | 37%  | 38%  | 36%  | 34%  | 35%  | 33%  | 33%  | 33%  | 32%  | 29%  | 30%  | 29%  | 23%  | 24%  | 22%  | 26%  | 23%  | 25%  | 26%  |
| [E_Code_POA_1](#e_code_poa)                                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 23%  | 25%  | 1%   |
| [E_Code_2](#e_code)                                                       | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 14%  | 12%  | 17%  | 19%  |
| [E_Code_POA_2](#e_code_poa)                                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 12%  | 17%  | 0%   |
| [E_Code_3](#e_code)                                                       | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 6%   | 5%   | 8%   | 7%   |
| [E_Code_POA_3](#e_code_poa)                                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 5%   | 8%   | 0%   |
| [Principal_ICD_Procedure](#icd_procedure)                                 | X                             | 15%  | 17%  | 15%  | 16%  | 15%  | 18%  | 19%  | 19%  | 19%  | 17%  | 16%  | 14%  | 14%  | 13%  | 14%  | 13%  | 13%  | 12%  | 11%  | 9%   | 1%   | 0%   | 2%   |
| [Procedure_Date_Principal](#procedure_date)                               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   |
| [Secondary_ICD_Procedure_1](#icd_procedure)                               | X                             | 3%   | 4%   | 3%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 1%   | 2%   | 1%   | 2%   | 1%   | 2%   | 1%   | 1%   | 1%   | 1%   | 1%   | 0%   | 0%   | 0%   |
| [Procedure_Date_Secondary_1](#procedure_date)                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_ICD_Procedure_2](#icd_procedure)                               | X                             | 1%   | 1%   | 1%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Procedure_Date_Secondary_2](#procedure_date)                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_ICD_Procedure_3](#icd_procedure)                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Procedure_Date_Secondary_3](#procedure_date)                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_ICD_Procedure_4](#icd_procedure)                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Procedure_Date_Secondary_4](#procedure_date)                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_ICD_Procedure_5](#icd_procedure)                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Procedure_Date_Secondary_5](#procedure_date)                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Diagnosis_Related_Group](#diagnosis_related_group)                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   |
| [Provider_Attending_ID](#provider_id)                                     |                               | 15%  | 6%   | 73%  | 82%  | 92%  | 45%  | 46%  | 45%  | 46%  | 46%  | 46%  | 46%  | 45%  | 43%  | 42%  | 55%  | 96%  | 95%  | 94%  | 89%  | 73%  | 74%  | 66%  |
| [Provider_Attending_NPI](#provider_npi)                                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   | 1%   | 2%   | 1%   | 66%  | 99%  | 99%  | 100% |
| [Provider_Attending_ID_Qualifier](#provider_id_qualifier)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   | 0%   | 1%   | 0%   | 61%  | 79%  | 79%  | 81%  |
| [Provider_Attending_Taxonomy_Code](#provider_taxonomy_code)               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 4%   | 16%  | 42%  | 0%   |
| [Provider_Attending_AMA_Specialty](#provider_ama_specialty)               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Attending_Other_Type_Qualifier](#provider_other_type_qualifier) |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Operating_ID](#provider_id)                                     |                               | 1%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 6%   | 3%   | 5%   | 50%  |
| [Provider_Operating_NPI](#provider_npi)                                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 6%   | 3%   | 5%   | 54%  |
| [Provider_Operating_ID_Qualifier](#provider_id_qualifier)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   | 0%   | 1%   | 0%   | 6%   | 3%   | 5%   | 54%  |
| [Provider_Operating_Taxonomy_Code](#provider_taxonomy_code)               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   | 0%   |
| [Provider_Operating_AMA_Specialty](#provider_ama_specialty)               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Operating_Other_Type_Qualifier](#provider_other_type_qualifier) |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_1_ID](#provider_id)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 50%  | 0%   | 0%   | 4%   |
| [Provider_Other_1_NPI](#provider_npi)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 51%  | 0%   | 0%   | 6%   |
| [Provider_Other_1_ID_Qualifier](#provider_id_qualifier)                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   | 0%   | 1%   | 0%   | 50%  | 0%   | 0%   | 13%  |
| [Provider_Other_1_Taxonomy_Code](#provider_taxonomy_code)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_1_AMA_Specialty](#provider_ama_specialty)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_1_Other_Type_Qualifier](#provider_other_type_qualifier)   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 4%   |
| [Provider_Other_2_ID](#provider_id)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 6%   |
| [Provider_Other_2_NPI](#provider_npi)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 7%   | 2%   | 2%   | 5%   |
| [Provider_Other_2_ID_Qualifier](#provider_id_qualifier)                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   | 1%   | 1%   | 11%  |
| [Provider_Other_2_Taxonomy_Code](#provider_taxonomy_code)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 0%   |
| [Provider_Other_2_AMA_Specialty](#provider_ama_specialty)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_2_Other_Type_Qualifier](#provider_other_type_qualifier)   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 8%   |
| [Provider_Other_3_ID](#provider_id)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 3%   | 31%  | 11%  | 0%   |
| [Provider_Other_3_NPI](#provider_npi)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 34%  | 13%  | 0%   |
| [Provider_Other_3_ID_Qualifier](#provider_id_qualifier)                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 34%  | 12%  | 0%   |
| [Provider_Other_3_Taxonomy_Code](#provider_taxonomy_code)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   | 0%   |
| [Provider_Other_3_AMA_Specialty](#provider_ama_specialty)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_3_Other_Type_Qualifier](#provider_other_type_qualifier)   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_ID](#provider_id)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_NPI](#provider_npi)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_ID_Qualifier](#provider_id_qualifier)                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_Taxonomy_Code](#provider_taxonomy_code)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_AMA_Specialty](#provider_ama_specialty)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_Other_Type_Qualifier](#provider_other_type_qualifier)   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Data_Source](#data_source)                                               |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 0%   | 0%   | 100% | 100% |
| [Service_Line](#service_line)                                             | X (AS only)                   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 80%  | 94%  | 94%  | 94%  |
| [Revenue_Code](#revenue_code)                                             | X (AS only)                   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% | 100% | 100% | 100% |
| [HCPCS_Code](#hcpcs_code)                                                 | X (AS only)                   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 13%  | 64%  | 87%  | 100% |
| [HCPCS_Code_Mod1](#hcpcs_code_mod)                                        | X (AS only)                   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 4%   | 26%  | 44%  | 45%  |
| [HCPCS_Code_Mod2](#hcpcs_code_mod)                                        | X (AS only)                   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 3%   |
| [HCPCS_Code_Mod3](#hcpcs_code_mod)                                        |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [HCPCS_Code_Mod4](#hcpcs_code_mod)                                        |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [National_Drug_Code](#national_drug_code)                                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 61%  |
| [Service_Line_Date](#service_line_date)                                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% |
| [Service_Units](#service_units)                                           |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 58%  | 100% | 100% | 100% |
| [Unit_Of_Measurement](#unit_of_measurement)                               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 67%  | 100% | 100% | 91%  |
| [Total_Charge_By_Revenue_Code](#total_charge_by_revenue_code)             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 80%  | 94%  | 94%  | 94%  |
| [ER_Charges](#er_charges)                                                 | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 80%  | 94%  | 94%  | 94%  |
| [Facility_Charges](#facility_charges)                                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 80%  | 94%  | 94%  | 94%  |
| [Professional_Charges](#professional_charges)                             | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 80%  | 94%  | 94%  | 94%  |
| [MSGMCE_Version_Used](#msgmce_version_used)                               | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Initial_DRG](#initial_drg)                                               |                               | 0%   | 0%   | 25%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |
| [Initial_MS_Indicator](#initial_ms_indicator)                             |                               | 0%   | 0%   | 25%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |
| [Final_MDC](#final_mdc)                                                   | X                             | 0%   | 0%   | 25%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |
| [Final_DRG](#final_drg)                                                   | X                             | 0%   | 0%   | 25%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |
| [Final_MS_Indicator](#final_ms_indicator)                                 | X                             | 0%   | 0%   | 25%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |
| [DRG_Return_Code](#drg_return_code)                                       | X                             | 100% | 100% | 75%  | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [MSGMCE_Edit_Return_Code](#msgmce_edit_return_code)                       | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Initial_4digit_DRG](#initial_4digit_drg)                                 | X                             | 0%   | 0%   | 25%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |
| [Final_4digit_DRG](#final_4digit_drg)                                     | X                             | 0%   | 0%   | 25%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |
| [Final_DRG_CC_MCC_Usage](#final_drg_cc_mcc_usage)                         | X                             | 0%   | 0%   | 1%   | 4%   | 4%   | 5%   | 5%   | 5%   | 6%   | 6%   | 7%   | 6%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 3%   | 4%   | 4%   |
| [Initial_DRG_CC_MCC_Usage](#initial_drg_cc_mcc_usage)                     | X                             | 0%   | 0%   | 1%   | 4%   | 4%   | 5%   | 5%   | 5%   | 6%   | 6%   | 7%   | 6%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 3%   | 4%   | 4%   |
| [Hospital_Acquired_Conditions](#hospital_acquired_conditions)             | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Hospital_Acquired_Condition_Status](#hospital_acquired_condition_status) |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Cost_Weight](#cost_weight)                                               | X                             | 0%   | 0%   | 25%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |

### Inpatient

|                               Variable Name                               | Standard Limited-Use Variable | 1996 | 1997 | 1998 | 1999 | 2000 | 2001 | 2002 | 2003 | 2004 | 2005 | 2006 | 2007 | 2008 | 2009 | 2010 | 2011 | 2012 | 2013 | 2014 | 2015 | 2016 | 2017 | 2018 |
| ------------------------------------------------------------------------- | ----------------------------- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| [Record_ID](#record_id)                                                   | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Encounter_Type](#encounter_type)                                         | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Facility_Number](#facility_number)                                       | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Facility_Name](#facility_name)                                           | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Age_Group_Num](#age_group_num)                                           | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Age_Group](#age_group)                                                   | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Bill_Type](#bill_type)                                                   |                               | 100% | 100% | 100% | 99%  | 77%  | 33%  | 32%  | 32%  | 32%  | 32%  | 33%  | 32%  | 32%  | 31%  | 31%  | 31%  | 31%  | 31%  | 31%  | 35%  | 53%  | 90%  | 100% |
| [ER_Flag](#er_flag)                                                       | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Observation_Stay_Flag](#observation_stay_flag)                           | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Year](#year)                                                             | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Quarter](#quarter)                                                       | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Patient_Medical_Record_Number](#patient_medical_record_number)           |                               | 100% | 99%  | 99%  | 91%  | 99%  | 89%  | 88%  | 88%  | 90%  | 90%  | 91%  | 91%  | 88%  | 88%  | 90%  | 90%  | 91%  | 91%  | 92%  | 92%  | 92%  | 97%  | 100% |
| [Patient_Social_Security_Number](#patient_social_security_number)         |                               | 13%  | 6%   | 11%  | 80%  | 80%  | 78%  | 80%  | 79%  | 79%  | 79%  | 81%  | 81%  | 79%  | 77%  | 79%  | 80%  | 80%  | 80%  | 80%  | 83%  | 80%  | 80%  | 80%  |
| [Patient_Social_Security_Flag](#patient_social_security_flag)             |                               | 1%   | 0%   | 0%   | 18%  | 17%  | 21%  | 19%  | 20%  | 19%  | 19%  | 18%  | 18%  | 19%  | 22%  | 20%  | 20%  | 19%  | 19%  | 19%  | 17%  | 16%  | 14%  | 0%   |
| [Patient_Unique_Number](#patient_unique_number)                           |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 0%   |
| [Patient_Control_Number](#patient_control_number)                         |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Patient_Name_Last](#patient_name_last)                                   |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Patient_Name_First](#patient_name_first)                                 |                               | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Patient_Name_Middle](#patient_name_middle)                               |                               | 34%  | 36%  | 40%  | 42%  | 39%  | 56%  | 60%  | 60%  | 61%  | 63%  | 64%  | 64%  | 64%  | 64%  | 66%  | 68%  | 68%  | 68%  | 68%  | 68%  | 66%  | 66%  | 64%  |
| [Patient_Name_Suffix](#patient_name_suffix)                               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   |
| [Patient_Street_Address](#patient_street_address)                         |                               | 14%  | 22%  | 24%  | 25%  | 23%  | 11%  | 10%  | 9%   | 10%  | 7%   | 8%   | 9%   | 9%   | 8%   | 8%   | 6%   | 6%   | 11%  | 24%  | 35%  | 66%  | 90%  | 99%  |
| [Patient_City](#patient_city)                                             |                               | 14%  | 22%  | 23%  | 24%  | 23%  | 11%  | 10%  | 9%   | 10%  | 7%   | 8%   | 9%   | 9%   | 8%   | 8%   | 6%   | 6%   | 11%  | 24%  | 35%  | 66%  | 90%  | 100% |
| [Patient_State](#patient_state)                                           | X                             | 14%  | 23%  | 24%  | 25%  | 23%  | 11%  | 10%  | 10%  | 10%  | 7%   | 8%   | 9%   | 9%   | 8%   | 8%   | 6%   | 7%   | 11%  | 24%  | 35%  | 98%  | 99%  | 100% |
| [Patient_County_FIPS](#patient_county_fips)                               | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Patient_Zip_Code](#patient_zip_code)                                     | X                             | 99%  | 99%  | 99%  | 99%  | 99%  | 100% | 100% | 99%  | 99%  | 97%  | 97%  | 98%  | 98%  | 98%  | 98%  | 98%  | 98%  | 98%  | 98%  | 100% | 100% | 100% | 97%  |
| [Patient_County](#patient_county)                                         |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 0%   | 92%  | 92%  | 84%  |
| [Patient_Country](#patient_country)                                       | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 14%  | 99%  | 99%  | 74%  |
| [Patient_Date_of_Birth](#patient_date_of_birth)                           |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Patient_Gender](#patient_gender)                                         | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Patient_Marital_Status](#patient_marital_status)                         | X                             | 86%  | 95%  | 95%  | 92%  | 92%  | 45%  | 44%  | 44%  | 44%  | 43%  | 44%  | 42%  | 40%  | 39%  | 39%  | 58%  | 99%  | 98%  | 99%  | 100% | 99%  | 98%  | 100% |
| [Patient_Race](#patient_race)                                             |                               | 85%  | 84%  | 80%  | 88%  | 87%  | 43%  | 42%  | 41%  | 41%  | 41%  | 92%  | 93%  | 94%  | 95%  | 96%  | 96%  | 95%  | 94%  | 96%  | 98%  | 100% | 97%  | 99%  |
| [Patient_Ethnicity](#patient_ethnicity)                                   |                               | 85%  | 84%  | 80%  | 87%  | 67%  | 28%  | 28%  | 27%  | 28%  | 29%  | 80%  | 80%  | 83%  | 85%  | 88%  | 90%  | 88%  | 88%  | 91%  | 100% | 100% | 73%  | 99%  |
| [Patient_Race_Ethnicity](#patient_race_ethnicity)                         | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Admission_Date](#admission_date)                                         |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [LOS_Days](#los_days)                                                     | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [LOS_Hours](#los_hours)                                                   | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Admission_Hour](#admission_hour)                                         | X                             | 8%   | 28%  | 92%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 96%  | 96%  | 97%  | 96%  | 95%  | 96%  | 96%  | 96%  | 97%  | 97%  |
| [Admission_Minute](#admission_minute)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 98%  | 98%  | 98%  | 93%  |
| [Admission_Type](#admission_type)                                         | X                             | 100% | 100% | 100% | 100% | 100% | 49%  | 48%  | 48%  | 48%  | 48%  | 48%  | 47%  | 46%  | 45%  | 43%  | 44%  | 44%  | 44%  | 45%  | 100% | 100% | 100% | 100% |
| [Admission_Source](#admission_source)                                     | X                             | 14%  | 19%  | 92%  | 100% | 100% | 49%  | 48%  | 48%  | 48%  | 48%  | 48%  | 47%  | 46%  | 45%  | 43%  | 44%  | 44%  | 44%  | 45%  | 100% | 100% | 100% | 100% |
| [Discharge_Hour](#discharge_hour)                                         |                               | 8%   | 9%   | 76%  | 94%  | 93%  | 95%  | 95%  | 96%  | 97%  | 96%  | 97%  | 96%  | 97%  | 95%  | 95%  | 96%  | 95%  | 93%  | 94%  | 47%  | 64%  | 86%  | 95%  |
| [Discharge_Minute](#discharge_minute)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 44%  | 48%  | 71%  | 76%  |
| [Patient_Discharge_Status](#patient_discharge_status)                     | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Patient_Discharge_Status_Uniform](#patient_discharge_status_uniform)     |                               |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Statement_Beginning_Date](#statement_beginning_date)                     |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Statement_Through_Date](#statement_through_date)                         |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Condition_Code_1](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 28%  | 51%  | 0%   |
| [Condition_Code_2](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_3](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_4](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_5](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_6](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_7](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_8](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_9](#condition_code)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_10](#condition_code)                                      |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Condition_Code_11](#condition_code)                                      |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Total_Charge_Header](#total_charge_header)                               | X                             | 95%  | 98%  | 93%  | 97%  | 98%  | 99%  | 99%  | 99%  | 99%  | 99%  | 99%  | 99%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Payer_Primary_Name](#payer_name)                                         | X                             | 77%  | 83%  | 97%  | 100% | 96%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 96%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 98%  | 97%  | 99%  |
| [Payer_Primary_ID](#payer_id)                                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 67%  |
| [Payer_Primary_Typology](#payer_typology)                                 | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% |
| [Payer_Primary_Typology_Imputed](#payer_typology_imputed)                 | X                             |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Estimated_Amount_Due_Primary](#estimated_amount_due)                     |                               | 0%   | 0%   | 0%   | 7%   | 8%   | 6%   | 4%   | 5%   | 5%   | 4%   | 5%   | 6%   | 5%   | 4%   | 4%   | 4%   | 3%   | 2%   | 3%   | 17%  | 23%  | 38%  | 40%  |
| [Prior_Payment_Primary](#prior_payment)                                   |                               | 0%   | 0%   | 0%   | 1%   | 18%  | 12%  | 13%  | 12%  | 13%  | 14%  | 14%  | 12%  | 13%  | 12%  | 11%  | 11%  | 10%  | 10%  | 11%  | 49%  | 49%  | 62%  | 62%  |
| [Payer_Secondary_Name](#payer_name)                                       |                               | 77%  | 83%  | 97%  | 100% | 96%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 96%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 98%  | 97%  | 99%  |
| [Payer_Secondary_ID](#payer_id)                                           |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 67%  |
| [Payer_Secondary_Typology](#payer_typology)                               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% |
| [Payer_Secondary_Typology_Imputed](#payer_typology_imputed)               |                               |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Estimated_Amount_Due_Secondary](#estimated_amount_due)                   |                               | 0%   | 0%   | 0%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 0%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 1%   | 0%   | 1%   | 2%   | 7%   | 17%  | 25%  |
| [Prior_Payment_Secondary](#prior_payment)                                 |                               | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 1%   | 1%   | 1%   | 2%   | 2%   | 1%   | 2%   | 1%   | 1%   | 1%   | 1%   | 1%   | 2%   | 11%  | 13%  | 19%  | 23%  |
| [Payer_Tertiary_Name](#payer_name)                                        |                               | 77%  | 83%  | 97%  | 100% | 96%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 96%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 97%  | 98%  | 97%  | 99%  |
| [Payer_Tertiary_ID](#payer_id)                                            |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 67%  |
| [Payer_Tertiary_Typology](#payer_typology)                                |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% |
| [Payer_Tertiary_Typology_Imputed](#payer_typology_imputed)                |                               |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| [Estimated_Amount_Due_Tertiary](#estimated_amount_due)                    |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 1%   |
| [Prior_Payment_Tertiary](#prior_payment)                                  |                               | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   | 1%   | 1%   | 1%   |
| [Insured_Primary_Unique_ID](#insured_unique_id)                           |                               | 11%  | 18%  | 82%  | 87%  | 89%  | 44%  | 43%  | 43%  | 44%  | 43%  | 44%  | 42%  | 41%  | 40%  | 39%  | 39%  | 37%  | 39%  | 40%  | 14%  | 60%  | 79%  | 88%  |
| [Insured_Primary_Name_Last](#insured_name_last)                           |                               | 11%  | 39%  | 86%  | 91%  | 92%  | 45%  | 44%  | 44%  | 44%  | 44%  | 44%  | 43%  | 42%  | 40%  | 38%  | 37%  | 35%  | 35%  | 40%  | 45%  | 61%  | 82%  | 0%   |
| [Insured_Primary_Name_First](#insured_name_first)                         |                               | 11%  | 39%  | 86%  | 91%  | 92%  | 45%  | 44%  | 44%  | 44%  | 44%  | 44%  | 43%  | 42%  | 40%  | 38%  | 37%  | 35%  | 35%  | 40%  | 45%  | 61%  | 82%  | 0%   |
| [Insured_Primary_Name_Middle](#insured_name_middle)                       |                               | 3%   | 25%  | 34%  | 37%  | 36%  | 18%  | 18%  | 19%  | 21%  | 23%  | 24%  | 24%  | 23%  | 22%  | 22%  | 23%  | 22%  | 22%  | 24%  | 27%  | 37%  | 53%  | 0%   |
| [Insured_Primary_Name_Suffix](#insured_name_suffix)                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   |
| [Insured_Primary_Group_Name](#insured_group_name)                         |                               | 3%   | 2%   | 11%  | 15%  | 18%  | 9%   | 9%   | 9%   | 8%   | 7%   | 7%   | 8%   | 8%   | 6%   | 5%   | 7%   | 7%   | 9%   | 10%  | 11%  | 10%  | 11%  | 0%   |
| [Patient_Relationship_Primary](#patient_relationship)                     |                               | 10%  | 70%  | 82%  | 88%  | 90%  | 45%  | 81%  | 94%  | 92%  | 94%  | 94%  | 94%  | 94%  | 94%  | 94%  | 95%  | 96%  | 95%  | 95%  | 89%  | 95%  | 93%  | 0%   |
| [Insured_Secondary_Unique_ID](#insured_unique_id)                         |                               | 11%  | 18%  | 82%  | 87%  | 89%  | 44%  | 43%  | 43%  | 44%  | 43%  | 44%  | 42%  | 41%  | 40%  | 39%  | 39%  | 37%  | 39%  | 40%  | 14%  | 60%  | 79%  | 88%  |
| [Insured_Secondary_Name_Last](#insured_name_last)                         |                               | 11%  | 39%  | 86%  | 91%  | 92%  | 45%  | 44%  | 44%  | 44%  | 44%  | 44%  | 43%  | 42%  | 40%  | 38%  | 37%  | 35%  | 35%  | 40%  | 45%  | 61%  | 82%  | 0%   |
| [Insured_Secondary_Name_First](#insured_name_first)                       |                               | 11%  | 39%  | 86%  | 91%  | 92%  | 45%  | 44%  | 44%  | 44%  | 44%  | 44%  | 43%  | 42%  | 40%  | 38%  | 37%  | 35%  | 35%  | 40%  | 45%  | 61%  | 82%  | 0%   |
| [Insured_Secondary_Name_Middle](#insured_name_middle)                     |                               | 3%   | 25%  | 34%  | 37%  | 36%  | 18%  | 18%  | 19%  | 21%  | 23%  | 24%  | 24%  | 23%  | 22%  | 22%  | 23%  | 22%  | 22%  | 24%  | 27%  | 37%  | 53%  | 0%   |
| [Insured_Secondary_Name_Suffix](#insured_name_suffix)                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   |
| [Insured_Secondary_Group_Name](#insured_group_name)                       |                               | 3%   | 2%   | 11%  | 15%  | 18%  | 9%   | 9%   | 9%   | 8%   | 7%   | 7%   | 8%   | 8%   | 6%   | 5%   | 7%   | 7%   | 9%   | 10%  | 11%  | 10%  | 11%  | 0%   |
| [Patient_Relationship_Secondary](#patient_relationship)                   |                               | 2%   | 67%  | 35%  | 37%  | 37%  | 16%  | 25%  | 32%  | 33%  | 37%  | 39%  | 41%  | 42%  | 44%  | 42%  | 46%  | 46%  | 46%  | 43%  | 62%  | 48%  | 30%  | 0%   |
| [Insured_Tertiary_Unique_ID](#insured_unique_id)                          |                               | 11%  | 18%  | 82%  | 87%  | 89%  | 44%  | 43%  | 43%  | 44%  | 43%  | 44%  | 42%  | 41%  | 40%  | 39%  | 39%  | 37%  | 39%  | 40%  | 14%  | 60%  | 79%  | 88%  |
| [Insured_Tertiary_Name_Last](#insured_name_last)                          |                               | 11%  | 39%  | 86%  | 91%  | 92%  | 45%  | 44%  | 44%  | 44%  | 44%  | 44%  | 43%  | 42%  | 40%  | 38%  | 37%  | 35%  | 35%  | 40%  | 45%  | 61%  | 82%  | 0%   |
| [Insured_Tertiary_Name_First](#insured_name_first)                        |                               | 11%  | 39%  | 86%  | 91%  | 92%  | 45%  | 44%  | 44%  | 44%  | 44%  | 44%  | 43%  | 42%  | 40%  | 38%  | 37%  | 35%  | 35%  | 40%  | 45%  | 61%  | 82%  | 0%   |
| [Insured_Tertiary_Name_Middle](#insured_name_middle)                      |                               | 3%   | 25%  | 34%  | 37%  | 36%  | 18%  | 18%  | 19%  | 21%  | 23%  | 24%  | 24%  | 23%  | 22%  | 22%  | 23%  | 22%  | 22%  | 24%  | 27%  | 37%  | 53%  | 0%   |
| [Insured_Tertiary_Name_Suffix](#insured_name_suffix)                      |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   |
| [Insured_Tertiary_Group_Name](#insured_group_name)                        |                               | 3%   | 2%   | 11%  | 15%  | 18%  | 9%   | 9%   | 9%   | 8%   | 7%   | 7%   | 8%   | 8%   | 6%   | 5%   | 7%   | 7%   | 9%   | 10%  | 11%  | 10%  | 11%  | 0%   |
| [Patient_Relationship_Tertiary](#patient_relationship)                    |                               | 0%   | 66%  | 14%  | 15%  | 14%  | 5%   | 6%   | 10%  | 11%  | 11%  | 14%  | 15%  | 15%  | 16%  | 16%  | 20%  | 21%  | 19%  | 13%  | 52%  | 35%  | 12%  | 0%   |
| [Diagnosis_Version_Flag](#diagnosis_version_flag)                         | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Principal_Diagnosis_Code](#principal_diagnosis_code)                     | X                             | 99%  | 98%  | 99%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |
| [Principal_Diagnosis_Code_POA](#principal_diagnosis_code_poa)             | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% | 100% | 2%   |
| [Secondary_Diagnosis_Code_1](#diagnosis_code)                             | X                             | 40%  | 44%  | 46%  | 52%  | 53%  | 63%  | 64%  | 66%  | 67%  | 68%  | 70%  | 71%  | 70%  | 71%  | 75%  | 75%  | 76%  | 75%  | 77%  | 77%  | 75%  | 73%  | 79%  |
| [Secondary_Diagnosis_Code_POA_1](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 75%  | 73%  | 2%   |
| [Secondary_Diagnosis_Code_2](#diagnosis_code)                             | X                             | 13%  | 16%  | 20%  | 24%  | 21%  | 38%  | 39%  | 41%  | 42%  | 43%  | 45%  | 46%  | 47%  | 47%  | 52%  | 51%  | 52%  | 51%  | 53%  | 54%  | 49%  | 47%  | 56%  |
| [Secondary_Diagnosis_Code_POA_2](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 49%  | 47%  | 1%   |
| [Secondary_Diagnosis_Code_3](#diagnosis_code)                             | X                             | 4%   | 5%   | 7%   | 8%   | 7%   | 15%  | 17%  | 19%  | 21%  | 22%  | 24%  | 25%  | 26%  | 27%  | 34%  | 33%  | 32%  | 33%  | 34%  | 35%  | 31%  | 29%  | 38%  |
| [Secondary_Diagnosis_Code_POA_3](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 31%  | 29%  | 1%   |
| [Secondary_Diagnosis_Code_4](#diagnosis_code)                             | X                             | 1%   | 2%   | 2%   | 3%   | 2%   | 6%   | 8%   | 9%   | 11%  | 12%  | 14%  | 14%  | 14%  | 16%  | 21%  | 20%  | 19%  | 20%  | 21%  | 21%  | 19%  | 18%  | 23%  |
| [Secondary_Diagnosis_Code_POA_4](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 19%  | 18%  | 1%   |
| [Secondary_Diagnosis_Code_5](#diagnosis_code)                             | X                             | 1%   | 1%   | 1%   | 1%   | 1%   | 3%   | 4%   | 5%   | 6%   | 7%   | 8%   | 8%   | 8%   | 9%   | 12%  | 11%  | 10%  | 11%  | 12%  | 13%  | 11%  | 11%  | 15%  |
| [Secondary_Diagnosis_Code_POA_5](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 11%  | 11%  | 0%   |
| [Secondary_Diagnosis_Code_6](#diagnosis_code)                             | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 2%   | 3%   | 4%   | 4%   | 5%   | 5%   | 5%   | 5%   | 8%   | 6%   | 6%   | 6%   | 7%   | 7%   | 7%   | 7%   | 9%   |
| [Secondary_Diagnosis_Code_POA_6](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 7%   | 7%   | 0%   |
| [Secondary_Diagnosis_Code_7](#diagnosis_code)                             | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 1%   | 2%   | 2%   | 3%   | 3%   | 3%   | 3%   | 5%   | 4%   | 3%   | 4%   | 4%   | 5%   | 4%   | 5%   | 6%   |
| [Secondary_Diagnosis_Code_POA_7](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 4%   | 5%   | 0%   |
| [Secondary_Diagnosis_Code_8](#diagnosis_code)                             | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 1%   | 2%   | 2%   | 2%   | 2%   | 3%   | 2%   | 2%   | 2%   | 3%   | 3%   | 3%   | 3%   | 4%   |
| [Secondary_Diagnosis_Code_POA_8](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 3%   | 3%   | 0%   |
| [Secondary_Diagnosis_Code_9](#diagnosis_code)                             | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 2%   | 2%   | 3%   |
| [Secondary_Diagnosis_Code_POA_9](#diagnosis_code_poa)                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   | 2%   | 0%   |
| [Secondary_Diagnosis_Code_10](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 2%   | 2%   |
| [Secondary_Diagnosis_Code_POA_10](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 2%   | 0%   |
| [Secondary_Diagnosis_Code_11](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 2%   |
| [Secondary_Diagnosis_Code_POA_11](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 0%   |
| [Secondary_Diagnosis_Code_12](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   |
| [Secondary_Diagnosis_Code_POA_12](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   |
| [Secondary_Diagnosis_Code_13](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   |
| [Secondary_Diagnosis_Code_POA_13](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   |
| [Secondary_Diagnosis_Code_14](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   |
| [Secondary_Diagnosis_Code_POA_14](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_15](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_POA_15](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_16](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_POA_16](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_17](#diagnosis_code)                            | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_Diagnosis_Code_POA_17](#diagnosis_code_poa)                    | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Admitting_Diagnosis_Code](#diagnosis_code)                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 59%  | 64%  | 55%  | 76%  |
| [Patient_Reason_For_Visit_1](#patient_reason_for_visit)                   | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 62%  | 80%  | 79%  | 80%  |
| [Patient_Reason_For_Visit_2](#patient_reason_for_visit)                   | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 11%  | 14%  | 17%  | 17%  |
| [Patient_Reason_For_Visit_3](#patient_reason_for_visit)                   | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 4%   | 5%   | 8%   | 7%   |
| [E_Code_1](#e_code)                                                       | X                             | 38%  | 34%  | 37%  | 38%  | 37%  | 38%  | 36%  | 34%  | 35%  | 33%  | 33%  | 33%  | 32%  | 29%  | 30%  | 29%  | 23%  | 24%  | 22%  | 26%  | 23%  | 25%  | 26%  |
| [E_Code_POA_1](#e_code_poa)                                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 23%  | 25%  | 1%   |
| [E_Code_2](#e_code)                                                       | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 14%  | 12%  | 17%  | 19%  |
| [E_Code_POA_2](#e_code_poa)                                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 12%  | 17%  | 0%   |
| [E_Code_3](#e_code)                                                       | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 6%   | 5%   | 8%   | 7%   |
| [E_Code_POA_3](#e_code_poa)                                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 5%   | 8%   | 0%   |
| [Principal_ICD_Procedure](#icd_procedure)                                 | X                             | 15%  | 17%  | 15%  | 16%  | 15%  | 18%  | 19%  | 19%  | 19%  | 17%  | 16%  | 14%  | 14%  | 13%  | 14%  | 13%  | 13%  | 12%  | 11%  | 9%   | 1%   | 0%   | 2%   |
| [Procedure_Date_Principal](#procedure_date)                               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   |
| [Secondary_ICD_Procedure_1](#icd_procedure)                               | X                             | 3%   | 4%   | 3%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 1%   | 2%   | 1%   | 2%   | 1%   | 2%   | 1%   | 1%   | 1%   | 1%   | 1%   | 0%   | 0%   | 0%   |
| [Procedure_Date_Secondary_1](#procedure_date)                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_ICD_Procedure_2](#icd_procedure)                               | X                             | 1%   | 1%   | 1%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Procedure_Date_Secondary_2](#procedure_date)                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_ICD_Procedure_3](#icd_procedure)                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Procedure_Date_Secondary_3](#procedure_date)                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_ICD_Procedure_4](#icd_procedure)                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Procedure_Date_Secondary_4](#procedure_date)                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Secondary_ICD_Procedure_5](#icd_procedure)                               | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Procedure_Date_Secondary_5](#procedure_date)                             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Diagnosis_Related_Group](#diagnosis_related_group)                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   |
| [Provider_Attending_ID](#provider_id)                                     |                               | 15%  | 6%   | 73%  | 82%  | 92%  | 45%  | 46%  | 45%  | 46%  | 46%  | 46%  | 46%  | 45%  | 43%  | 42%  | 55%  | 96%  | 95%  | 94%  | 89%  | 73%  | 74%  | 66%  |
| [Provider_Attending_NPI](#provider_npi)                                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   | 1%   | 2%   | 1%   | 66%  | 99%  | 99%  | 100% |
| [Provider_Attending_ID_Qualifier](#provider_id_qualifier)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   | 0%   | 1%   | 0%   | 61%  | 79%  | 79%  | 81%  |
| [Provider_Attending_Taxonomy_Code](#provider_taxonomy_code)               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 4%   | 16%  | 42%  | 0%   |
| [Provider_Attending_AMA_Specialty](#provider_ama_specialty)               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Attending_Other_Type_Qualifier](#provider_other_type_qualifier) |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Operating_ID](#provider_id)                                     |                               | 1%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 6%   | 3%   | 5%   | 50%  |
| [Provider_Operating_NPI](#provider_npi)                                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 6%   | 3%   | 5%   | 54%  |
| [Provider_Operating_ID_Qualifier](#provider_id_qualifier)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   | 0%   | 1%   | 0%   | 6%   | 3%   | 5%   | 54%  |
| [Provider_Operating_Taxonomy_Code](#provider_taxonomy_code)               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   | 0%   |
| [Provider_Operating_AMA_Specialty](#provider_ama_specialty)               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Operating_Other_Type_Qualifier](#provider_other_type_qualifier) |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_1_ID](#provider_id)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 50%  | 0%   | 0%   | 4%   |
| [Provider_Other_1_NPI](#provider_npi)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 51%  | 0%   | 0%   | 6%   |
| [Provider_Other_1_ID_Qualifier](#provider_id_qualifier)                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   | 0%   | 1%   | 0%   | 50%  | 0%   | 0%   | 13%  |
| [Provider_Other_1_Taxonomy_Code](#provider_taxonomy_code)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_1_AMA_Specialty](#provider_ama_specialty)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_1_Other_Type_Qualifier](#provider_other_type_qualifier)   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 4%   |
| [Provider_Other_2_ID](#provider_id)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 6%   |
| [Provider_Other_2_NPI](#provider_npi)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 7%   | 2%   | 2%   | 5%   |
| [Provider_Other_2_ID_Qualifier](#provider_id_qualifier)                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   | 1%   | 1%   | 11%  |
| [Provider_Other_2_Taxonomy_Code](#provider_taxonomy_code)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 0%   |
| [Provider_Other_2_AMA_Specialty](#provider_ama_specialty)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_2_Other_Type_Qualifier](#provider_other_type_qualifier)   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 8%   |
| [Provider_Other_3_ID](#provider_id)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 3%   | 31%  | 11%  | 0%   |
| [Provider_Other_3_NPI](#provider_npi)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 34%  | 13%  | 0%   |
| [Provider_Other_3_ID_Qualifier](#provider_id_qualifier)                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 34%  | 12%  | 0%   |
| [Provider_Other_3_Taxonomy_Code](#provider_taxonomy_code)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 2%   | 0%   |
| [Provider_Other_3_AMA_Specialty](#provider_ama_specialty)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_3_Other_Type_Qualifier](#provider_other_type_qualifier)   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_ID](#provider_id)                                       |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_NPI](#provider_npi)                                     |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_ID_Qualifier](#provider_id_qualifier)                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_Taxonomy_Code](#provider_taxonomy_code)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_AMA_Specialty](#provider_ama_specialty)                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Provider_Other_4_Other_Type_Qualifier](#provider_other_type_qualifier)   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Data_Source](#data_source)                                               |                               | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 0%   | 0%   | 100% | 100% |
| [Service_Line](#service_line)                                             | X (AS only)                   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 80%  | 94%  | 94%  | 94%  |
| [Revenue_Code](#revenue_code)                                             | X (AS only)                   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% | 100% | 100% | 100% |
| [HCPCS_Code](#hcpcs_code)                                                 | X (AS only)                   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 13%  | 64%  | 87%  | 100% |
| [HCPCS_Code_Mod1](#hcpcs_code_mod)                                        | X (AS only)                   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 4%   | 26%  | 44%  | 45%  |
| [HCPCS_Code_Mod2](#hcpcs_code_mod)                                        | X (AS only)                   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 1%   | 3%   |
| [HCPCS_Code_Mod3](#hcpcs_code_mod)                                        |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [HCPCS_Code_Mod4](#hcpcs_code_mod)                                        |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [National_Drug_Code](#national_drug_code)                                 |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 61%  |
| [Service_Line_Date](#service_line_date)                                   |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 100% |
| [Service_Units](#service_units)                                           |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 58%  | 100% | 100% | 100% |
| [Unit_Of_Measurement](#unit_of_measurement)                               |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 67%  | 100% | 100% | 91%  |
| [Total_Charge_By_Revenue_Code](#total_charge_by_revenue_code)             |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 80%  | 94%  | 94%  | 94%  |
| [ER_Charges](#er_charges)                                                 | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 80%  | 94%  | 94%  | 94%  |
| [Facility_Charges](#facility_charges)                                     | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 80%  | 94%  | 94%  | 94%  |
| [Professional_Charges](#professional_charges)                             | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 80%  | 94%  | 94%  | 94%  |
| [MSGMCE_Version_Used](#msgmce_version_used)                               | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Initial_DRG](#initial_drg)                                               |                               | 0%   | 0%   | 25%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |
| [Initial_MS_Indicator](#initial_ms_indicator)                             |                               | 0%   | 0%   | 25%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |
| [Final_MDC](#final_mdc)                                                   | X                             | 0%   | 0%   | 25%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |
| [Final_DRG](#final_drg)                                                   | X                             | 0%   | 0%   | 25%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |
| [Final_MS_Indicator](#final_ms_indicator)                                 | X                             | 0%   | 0%   | 25%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |
| [DRG_Return_Code](#drg_return_code)                                       | X                             | 100% | 100% | 75%  | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 1%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [MSGMCE_Edit_Return_Code](#msgmce_edit_return_code)                       | X                             | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% |
| [Initial_4digit_DRG](#initial_4digit_drg)                                 | X                             | 0%   | 0%   | 25%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |
| [Final_4digit_DRG](#final_4digit_drg)                                     | X                             | 0%   | 0%   | 25%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |
| [Final_DRG_CC_MCC_Usage](#final_drg_cc_mcc_usage)                         | X                             | 0%   | 0%   | 1%   | 4%   | 4%   | 5%   | 5%   | 5%   | 6%   | 6%   | 7%   | 6%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 3%   | 4%   | 4%   |
| [Initial_DRG_CC_MCC_Usage](#initial_drg_cc_mcc_usage)                     | X                             | 0%   | 0%   | 1%   | 4%   | 4%   | 5%   | 5%   | 5%   | 6%   | 6%   | 7%   | 6%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 2%   | 3%   | 4%   | 4%   |
| [Hospital_Acquired_Conditions](#hospital_acquired_conditions)             | X                             | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Hospital_Acquired_Condition_Status](#hospital_acquired_condition_status) |                               | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   | 0%   |
| [Cost_Weight](#cost_weight)                                               | X                             | 0%   | 0%   | 25%  | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 100% | 99%  | 100% | 100% | 100% | 100% | 100% |

## Variable Descriptions

### Record_ID
A unique number for each visit, which is also unique across all years of available data.

### Encounter_Type
| Code |     Description      |
| ---- | -------------------- |
| A    | Ambulatory Surgery   |
| E    | Emergency Department |
| I    | Inpatient            |

### Facility_Number
OHCS assigned identifier for the facility from which the patient was discharged.

### Facility_Name
Name of the facility from which the patient was discharged.

### Age_Group_Num
Numeric code representing age of patient at date of release.

| Code | Description |
| ---- | ----------- |
| 0    | 1-28 days   |
| 1    | 29-365 days |
| 2    | 1-4 years   |
| 3    | 5-9         |
| 4    | 10-14       |
| 5    | 15-17       |
| 6    | 18-19       |
| 7    | 20-24       |
| 8    | 25-29       |
| 9    | 30-34       |
| 10   | 35-39       |
| 11   | 40-44       |
| 12   | 45-49       |
| 13   | 50-54       |
| 14   | 55-59       |
| 15   | 60-64       |
| 16   | 65-69       |
| 17   | 70-74       |
| 18   | 75-79       |
| 19   | 80-84       |
| 20   | 85-89       |
| 21   | 90 +        |
| 99   | Suppressed  |

### Age_Group
Text description for age of patient at date of release. See table for
[Age_Group_Num](#age_group_num).

### Bill_Type


### ER_Flag
Indicates whether the encounter included emergency room services, as indicated by
revenue codes in the 045X series.

### Observation_Stay_Flag
Indicates whether the encounter included observation stay services, as indicated by
revenue codes in the 076X series. 

### Year
Year of discharge. Derived from [Statement_Through_Date](#statement_through_date).

### Quarter
Quarter of discharge. Derived from [Statement_Through_Date](#statement_through_date).

### Patient_Medical_Record_Number
Patient Medical Record Number.

### Patient_Social_Security_Number
Patient Social Security Number.

### Patient_Social_Security_Flag
Discontinued starting with data submission CY2018.

### Patient_Unique_Number
Internally used database control number.

### Patient_Control_Number
Internally used database control number.

### Patient_Name_Last
Patient last name.

### Patient_Name_First
Patient first name.

### Patient_Name_Middle
Patient middle name.

### Patient_Name_Suffix
Discontinued starting with data submission CY2018.

Patient name suffix.

### Patient_Street_Address
Patient residential street address.

### Patient_City
Patient residential city.

### Patient_State
Patient residential state.

### Patient_County_FIPS
FIPS code for the county of residence. This field is derived from the patient’s ZIP code
using the Federal Department of Housing and Urban Development’s [ZIP-County
ratios](https://www.huduser.gov/portal/datasets/usps_crosswalk.html). If a ZIP crossed a
county boundary, all encounters in the ZIP were assigned to the county with the higher
proportion of the population.

### Patient_Zip_Code
Patient residential ZIP code.

### Patient_County
Deprecated variable. See [Patient_County_FIPS](#patient_county_fips).

### Patient_Country
Patient residential country code.

### Patient_Date_of_Birth
Patient date of birth.

### Patient_Gender
| Code | Description |
| ---- | ----------- |
| M    | Male        |
| F    | Female      |
| U    | Unknown     |
| S    | Suppressed  |

### Patient_Marital_Status
Data submitted starting CY2018.

| Code |    Description    |
| ---- | ----------------- |
| A    | Annulled          |
| D    | Divorced          |
| I    | Interlocutory     |
| L    | Legally Separated |
| M    | Married           |
| P    | Polygamous        |
| S    | Never Married     |
| T    | Domestic Partner  |
| U    | Unmarried         |
| W    | Widowed           |
| UNK  | Unknown           |

Data submitted prior to CY2018.

| Code |    Description    |
| ---- | ----------------- |
| S    | Single            |
| M    | Married           |
| X    | Legally Separated |
| D    | Divorced          |
| W    | Widowed           |
| P    | Life Partner      |

### Patient_Race
Data submitted starting CY2018. Based on the [HL7 FHIR race code system](https://www.hl7.org/fhir/v3/Race/cs.html).

|  Code  |                Description                |
| ------ | ----------------------------------------- |
| 1002-5 | American Indian/Alaska Native             |
| 2028-9 | Asian                                     |
| 2054-5 | Black/African American                    |
| 2106-3 | White                                     |
| 2131-1 | Other Race                                |
| 2076-8 | Native Hawaiian or Other Pacific Islander |

Data submitted prior to CY2018.

| Code |                Description                |
| ---- | ----------------------------------------- |
| 1    | American Indian/Alaska Native             |
| 2    | Asian                                     |
| 3    | Black/African American                    |
| 4    | White                                     |
| 5    | Other Race                                |
| 6    | Unknown                                   |
| 7    | Native Hawaiian or Other Pacific Islander |

### Patient_Ethnicity
Data submitted starting CY2018. Based on the [HL7 FHIR ethnicity code system](https://www.hl7.org/fhir/v3/Ethnicity/cs.html).

|  Code  |      Description       |
| ------ | ---------------------- |
| 2135-2 | Hispanic or Latino     |
| 2186-5 | Not Hispanic or Latino |

Data submitted prior to CY2018.

| Code |      Description       |
| ---- | ---------------------- |
| 1    | Hispanic or Latino     |
| 2    | Not Hispanic or Latino |
| 6    | Unknown                |

### Patient_Race_Ethnicity
Computed using [Patient_Race](#patient_race) and [patient_ethnicity](#patient_ethnicity).

| Code  |          Description           |
| ----- | ------------------------------ |
| W     | White, non-Hispanic origin     |
| WH    | White, Hispanic origin         |
| NW    | Non-white, Hispanic origin     |
| NH    | Non-white, non-Hispanic origin |
| UK    | Unknown                        |
| Blank | Not reported                   |

### Admission_Date
Admission date.

### LOS_Days
Total days stayed in hospital from the date of admission to the date of discharge.
Computed using Admission_Date/Admission_Hour/Admission_Minute and
Statement_Through_Date/Discharge_Hour/Discharge_Minute.

### LOS_Hours
Total hours stayed in hospital from the date of admission to the date of discharge.
Computed using Admission_Date/Admission_Hour/Admission_Minute and
Statement_Through_Date/Discharge_Hour/Discharge_Minute.

### Admission_Hour
The hour during which the patient arrived at the facility (using the 24-hour clock
format).

### Admission_Minute
The minute during which the patient arrived at the facility.

### Admission_Type
Please see the table on HCUP's [admission type](https://hcup-us.ahrq.gov/db/vars/atype/nisnote.jsp) website.

### Admission_Source
Please see the table on HCUP's [point of origin for admission or visit](https://hcup-us.ahrq.gov/db/vars/pointoforiginub04/nisnote.jsp) website.

### Discharge_Hour
The hour during which the patient departed the facility (using the 24-hour clock
format).

### Discharge_Minute
The minute during which the patient departed the facility.

### Patient_Discharge_Status
For discharges on or after October 1, 2007, please see the table on HCUP's [disposition of patient - UB04](https://hcup-us.ahrq.gov/db/vars/dispub04/nisnote.jsp) website.

For discharges prior to October 1, 2007, please see the table on HCUP's [disposition of patient - UB92](https://hcup-us.ahrq.gov/db/vars/dispub92/nisnote.jsp) website.

### Patient_Discharge_Status_Uniform
Adapted from HCUP's [uniform disposition of patient coding scheme](https://hcup-us.ahrq.gov/db/vars/dispuniform/nisnote.jsp).

| Code |                                                     Description                                                     |
| ---- | ------------------------------------------------------------------------------------------------------------------- |
| 1    | Routine                                                                                                             |
| 2    | Transfer to Short-term Hospital                                                                                     |
| 5    | Transfer Other: Includes Skilled Nursing Facility (SNF), Intermediate Care Facility (ICF), Another Type of Facility |
| 6    | Home Health Care (HHC)                                                                                              |
| 7    | Against Medical Advice (AMA)                                                                                        |
| 20   | Died                                                                                                                |
| 99   | Discharge alive, destination unknown                                                                                |
| -1   | Missing/Invalid                                                                                                     |

### Statement_Beginning_Date
Statement beginning date. The date hospital services began to be rendered.

### Statement_Through_Date
Statement_Through_Date. The date hospital services ended. By definition, the discharge date.

### Condition_Code
Discontinued starting with data submitted CY2018.

| Code |                                                                                         Description                                                                                         |
| ---- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 17   | Homeless or ZIP code unknown                                                                                                                                                                |
| P1   | Do Not Resuscitate (DNR) order was written at the time of or within the first 24 hours of the patient’s admission to the hospital and is clearly documented in the patient’s medical record |
| P7   | Admit from Emergency Room                                                                                                                                                                   |

### Total_Charge_Header
The total amount charged by the hospital for the hospital encounter.

### Payer_Name
Free-text payer name.

### Payer_ID
Available starting with data submitted CY2018.
Unique payer identifier issued by clearinghouse for EDI transactions. Blank for self-pay.

### Payer_Typology
Available starting with data submitted CY2018.

| Code |                             Description                             |                                                                                 Definition                                                                                  |
| ---- | ------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1    | Medicare                                                            | Medicare Managed Care, Medicare Fee for Service, Medicare Hospice, or Dual Eligibility Medicare/Medicaid Organization                                                       |
| 2    | Medicaid                                                            | Medicaid Managed Care, Medicaid Fee for Service, CHIP, Medicaid Long Term Care, or Medicaid Dental                                                                          |
| 3    | Other Government                                                    | Department of Defense (i.e. Tricare), Department of Veterans Affairs, Indian Health Service or Tribe, HRSA Program, Black Lung, State Government, or Local Government       |
| 4    | Department of Corrections                                           | Federal, State, or Local Corrections                                                                                                                                        |
| 5    | Private Health Insurance                                            | Managed Care (e.g. HMO, PPO, POS), Private Health Insurance (e.g. commercial indemnity or self-funded ERISA), Organized Delivery System, or Small Employer Purchasing Group |
| 6    | Blue Cross/Blue Shield                                              | BCBS Managed Care or BCBS Indemnity Insurance                                                                                                                               |
| 7    | Managed Care, Unspecified                                           | Only use if cannot distinguish public from private managed care.                                                                                                            |
| 8    | No Payment from an Organization/Agency/Program/Private Payer Listed | Self-Pay, No Charge, Charity, Refusal, Research/Donor, or No Payment                                                                                                        |
| 9    | Miscellaneous/Other                                                 | Workers Compensation, Foreign National, Disability, Long-Term Care, Auto Insurance, or Legal Liability                                                                      |
|      | Unknown                                                             | Leave blank if payer typology is unknown, unavailable, or blank.                                                                                                            |

### Payer_Typology_Imputed
Imputed payer typology using string-matching on [Payer_Name](#payer_name).

### Estimated_Amount_Due
The amount estimated by the hospital to be due from the indicated payer (estimated
responsibility less prior payments).

### Prior_Payment
The amount the hospital has received toward the payment prior to the billing date from the
indicated payer.

### Insured_Unique_ID
Policy or contract number assigned by the insurer.

### Insured_Name_Last
Discontinued starting with data submission CY2018.

Policy holder last name.

### Insured_Name_First
Discontinued starting with data submission CY2018.

Policy holder first name.

### Insured_Name_Middle
Discontinued starting with data submission CY2018.

Policy holder middle name.

### Insured_Name_Suffix
Discontinued starting with data submission CY2018.

Policy holder name suffix.

### Insured_Group_Name
Discontinued starting with data submission CY2018.

Policy holder insurance group name.

### Patient_Relationship
Discontinued starting with data submission CY2018.

Policy holder relationship-to-patient code.

### Diagnosis_Version_Flag
| Code |     Description      |
| ---- | -------------------- |
| 9    | ICD-9-CM             |
| 10   | ICD-10-CM/ICD-10-PCS |
| -1   | Unknown              |

### Diagnosis_Code
ICD diagnosis code.

For discharges prior to October 1, 2015, this field contains ICD-9 codes.
For discharges on or after October 1, 2015, this fieild contains ICD-10 codes.

### Diagnosis_Code_POA
Diagnosis code present-on-admission codes.

| Code  |                         Description                             |
| ----- | --------------------------------------------------------------- |
| Y     | Yes, present at the time of inpatient admission                 |
| N     | No, not present at the time of inpatient admission              |
| U     | Insufficient documentation to determine if present on admission |
| W     | Clinically unable to determine if present at time of admission  |
| 1     | Code is exempt from POA reporting                               |
| Blank | Code is exempt from POA reporting                               |

### Admitting_Diagnosis_Code
Admitting diagnosis code.

### Patient_Reason_For_Visit
Patient reason for visit, coded using ICD diagnosis codes.

### E_Code
External cause code, coded using ICD diagnosis codes.

### E_Code_POA
External cause code present-on-admission codes.

| Code  |                         Description                             |
| ----- | --------------------------------------------------------------- |
| Y     | Yes, present at the time of inpatient admission                 |
| N     | No, not present at the time of inpatient admission              |
| U     | Insufficient documentation to determine if present on admission |
| W     | Clinically unable to determine if present at time of admission  |
| 1     | Code is exempt from POA reporting                               |
| Blank | Code is exempt from POA reporting                               |

### ICD_Procedure
ICD procedure code.

For discharges prior to October 1, 2015, this field contains ICD-9 codes.
For discharges on or after October 1, 2015, this fieild contains ICD-10 codes.

### Procedure_Date
Date of ICD procedure.

### Diagnosis_Related_Group
Diagnosis Related Group (DRG) as reported by the hospital. May vary from calculated
[MS-DRG field](#final_drg).

### Provider_ID
Secondary provider identifer of the type indicated by [Provider_ID_Qualifier](#provider_id_qualifier).

### Provider_NPI
NPI for the provider as enumerated in [National Plan and Provider Enumeration System (NPPES)](https://nppes.cms.hhs.gov/).

### Provider_ID_Qualifier
Indicates the type of [Provider_ID](#provider_id).

| Code |           Description            |
| ---- | -------------------------------- |
| 0B   | State License Number             |
| 1G   | Provider UPIN Number             |
| G2   | Provider Commercial Number       |
| EI   | Employer’s Identification Number |
| SY   | Social Security Number           |

### Provider_Taxonomy_Code
Provider taxonomy code, coded using NUCC's [Health Care Provider taxonomy](http://nucc.org/index.php/code-sets-mainmenu-41/provider-taxonomy-mainmenu-40) code set.

### Provider_AMA_Specialty
Provider specialty, coded using AMA's specialty coding system.

### Provider_Other_Type_Qualifier
Indicates the type of "other" provider.

| Code |                                                             Description                                                             |
| ---- | ----------------------------------------------------------------------------------------------------------------------------------- |
| DN   | Referring Provider - The provider who sends the patient to another provider for services.                                           |
| ZZ   | Other Operating Physician - An individual performing a secondary surgical procedure or assisting the Operating Physician.           |
| 82   | Rendering Provider - The health care professional who delivers or completes a particular medical service or non-surgical procedure. |

### Data_Source
Internal OHCS filename.

### Service_Line
Count of services. Service lines represent revenue lines and the associated procedures and
charges. Header data repeats while service line data changes with each new line. Currently
only applies to ambulatory surgery records. That is, standard extracts of inpatient and ED encounters
only consist of a single record line, while ambulatory surgery encounters can consist of
multiple lines with incrementing service line values.

### Revenue_Code
Codes that identify specific accommodations, ancillary service or unique billing
calculations or arrangements. NUBC Code using leading zeroes, left justified, and four
digits. Revenue codes are maintained by NUBC.

### HCPCS_Code
Healthcare Common Procedural Coding System (HCPCS). This includes the CPT codes maintained
by the American Medical Association.

### HCPCS_Code_Mod
Procedure modifier required when a modifier clarifies/improves the reporting accuracy of
the associated procedure code.  CPT codes and modifiers are maintained by the American
Medical Association.

### National_Drug_Code
Available starting with data submitted CY2018.

### Service_Line_Date
Available starting with data submitted CY2018.

### Service_Units
The quantity of units, times, days, visits, services, or treatments for the service
described by the HCPCS/CPT Procedure Code or Revenue Code.

### Unit_of_Measurement
Unit of measurement. Indicates the type of [Service Units](#service_units), e.g., days.

### Total_Charge_By_Revenue_Code
The total amount charged by the hospital for the for the given service line.

### ER_Charges
The total amount charged by the hospital for emergency room services, as indicated by
revenue codes in the 045X series.

This field is slated to be removed in future releases.

### Facility_Charges
The total amount charged by the hospital for emergency room services, as indicated by
revenue codes between 0100 and 0959.

This field is slated to be removed in future releases.

### Professional_Charges
The total amount charged by the hospital for emergency room services, as indicated by
revenue codes between 0960 and 0989.

This field is slated to be removed in future releases.

### MSGMCE_Version_Used
MSGMCE software version used for assigning the MS-DRG.

### Initial_DRG
Initial DRG. Because no POA-flags are used in calculating the [Final DRG](#final_drg),
this will always be equal to the [Final DRG](#final_drg).

### Initial_MS_Indicator
Initial Medical/Surgical indicator. Because no POA-flags are used in calculating the [Final MS Indicator](#final_ms_indicator),
this will always be equal to the [Final MS Indicator](#final_ms_indicator).

### Final_MDC
Indicates the final Major Diagnostic Category assigned by the CMS grouper.

| Code |                                Description                                 |
| ---- | -------------------------------------------------------------------------- |
| 01   | Diseases & Disorders of the Nervous System                                 |
| 02   | Diseases & Disorders of the Eye                                            |
| 03   | Diseases & Disorders of the Ear, Nose, Mouth & Throat                      |
| 04   | Diseases & Disorders of the Respiratory System                             |
| 05   | Diseases & Disorders of the Circulatory System                             |
| 06   | Diseases & Disorders of the Digestive System                               |
| 07   | Diseases & Disorders of the Hepatobiliary System & Pancreas                |
| 08   | Diseases & Disorders of the Musculoskeletal System & Connective Tissue     |
| 09   | Diseases & Disorders of the Skin, Subcutaneous Tissue & Breast             |
| 10   | Endocrine, Nutritional & Metabolic Diseases & Disorders                    |
| 11   | Diseases & Disorders of the Kidney & Urinary Tract                         |
| 12   | Diseases & Disorders of the Male Reproductive System                       |
| 13   | Diseases & Disorders of the Female Reproductive System                     |
| 14   | Pregnancy, Childbirth & the Puerperium                                     |
| 15   | Newborns & Other Neonates with Conditions Originating in Perinatal Period  |
| 16   | Diseases & Disorders of Blood, Blood Forming Organs, Immunologic Disorders |
| 17   | Myeloproliferative Diseases & Disorders, Poorly Differentiated Neoplasms   |
| 18   | Infectious & Parasitic Diseases, Systemic or Unspecified Sites             |
| 19   | Mental Diseases & Disorders                                                |
| 20   | Alcohol/Drug Use & Alcohol/Drug Induced Organic Mental Disorders           |
| 21   | Injuries, Poisonings & Toxic Effects of Drugs                              |
| 22   | Burns                                                                      |
| 23   | Factors Influencing Health Status & Other Contacts with Health Services    |
| 24   | Multiple Significant Trauma                                                |
| 25   | Human Immunodeficiency Virus Infections                                    |

### Final_DRG
"Final" MS-DRG assigned by the MSGMCE software.

### Final_MS_Indicator
Final Medical/Surgical indicator.

| Code |      Description       |
| ---- | ---------------------- |
| 0    | Error DRG (998 or 999) |
| 1    | Medical DRG            |
| 2    | Surgical DRG           |

### DRG_Return_Code
See [MS-DRG grouper documentation](https://www.cms.gov/Medicare/Medicare-Fee-for-Service-Payment/AcuteInpatientPPS/MS-DRG-Classifications-and-Software) for details.

### MSGMCE_Edit_Return_Code
See [MS-DRG grouper documentation](https://www.cms.gov/Medicare/Medicare-Fee-for-Service-Payment/AcuteInpatientPPS/MS-DRG-Classifications-and-Software) for details.

### Initial_4digit_DRG
See [MS-DRG grouper documentation](https://www.cms.gov/Medicare/Medicare-Fee-for-Service-Payment/AcuteInpatientPPS/MS-DRG-Classifications-and-Software) for details.

### Final_4digit_DRG
See [MS-DRG grouper documentation](https://www.cms.gov/Medicare/Medicare-Fee-for-Service-Payment/AcuteInpatientPPS/MS-DRG-Classifications-and-Software) for details.

### Final_DRG_CC_MCC_Usage
See [MS-DRG grouper documentation](https://www.cms.gov/Medicare/Medicare-Fee-for-Service-Payment/AcuteInpatientPPS/MS-DRG-Classifications-and-Software) for details.

### Initial_DRG_CC_MCC_Usage
See [MS-DRG grouper documentation](https://www.cms.gov/Medicare/Medicare-Fee-for-Service-Payment/AcuteInpatientPPS/MS-DRG-Classifications-and-Software) for details.

### Hospital_Acquired_Conditions
See [MS-DRG grouper documentation](https://www.cms.gov/Medicare/Medicare-Fee-for-Service-Payment/AcuteInpatientPPS/MS-DRG-Classifications-and-Software) for details.

### Hospital_Acquired_Condition_Status
See [MS-DRG grouper documentation](https://www.cms.gov/Medicare/Medicare-Fee-for-Service-Payment/AcuteInpatientPPS/MS-DRG-Classifications-and-Software) for details.

### Cost_Weight
See [MS-DRG grouper documentation](https://www.cms.gov/Medicare/Medicare-Fee-for-Service-Payment/AcuteInpatientPPS/MS-DRG-Classifications-and-Software) for details.
